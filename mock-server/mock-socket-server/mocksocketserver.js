var io = require('socket.io')();
io.origins('*:*');

var theSocket;

setInterval(function() {
  if (theSocket) {
    theSocket.emit('num1', 'A' + Math.random()*1000);
  }
}, 500);
setInterval(function() {
  if (theSocket) {
    theSocket.emit('num2', 'B' + Math.random()*10000);
  }
}, 1000);
setInterval(function() {
  if (theSocket) {
    theSocket.emit('num3', 'C' + Math.random()*100000);
  }
}, 2000);
setInterval(function() {
  if (theSocket) {
    theSocket.emit('num4', 'D' + Math.random()*10000);
  }
}, 3000);

io.on('connection', function(socket){
  theSocket = socket;
});
io.listen(3001);
