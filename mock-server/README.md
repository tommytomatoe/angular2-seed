This mock REST API serves up generated mocked data (mockdb.json) from [json-generator.com](http://beta.json-generator.com). It should be running for the seed app's dashboard to display data.

It uses the [json-server](https://github.com/typicode/json-server) NPM package. 

To start the `json-server` independently, run: `npm run json-server`. 

Note: running `npm start` will automatically start the `json-server`.