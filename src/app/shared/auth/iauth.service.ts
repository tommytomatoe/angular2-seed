import {Observable} from "rxjs/Rx";

export interface LoggedInUser {
  id: string;
  email: string;
  isAdmin?: boolean;
  name?: string;
  imageUrl?: string;
  google?: {
    id: string
  }
}

export interface IAuthService {
  // Signs in a user given an email and a password.
  // For this mock service, any email or password are accepted.
  // Returns an observable with the user object with the idea
  // that your auth service will return an Angular HTTP post.
  signIn(email: string, password: string): Observable<LoggedInUser>;

  // If the user is logged in, returns them
  getLoggedInUser(): LoggedInUser;

  // Opens up a Google sign-in popup. Returns an observable with the
  // user object with the idea that your auth service will return an Angular HTTP post.
  signInWithGoogle(): Observable<LoggedInUser>;

  // Changes the logged in user's permission to normal. Meant as a
  // simple way to test admin routes and permissions.
  changeLoggedInUserPermissionToNormal(): void;

  // See changeLoggedInUserPermissionToNormal().
  changeLoggedInUserPermissionToAdmin(): void;
}
