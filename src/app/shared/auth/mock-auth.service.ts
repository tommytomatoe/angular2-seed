// This class implements IAuthService and can be used to sign in and sign out with either
// fake accounts or Google accounts. The user object is stored in local storage for mock purposes.

// Google's API, included in index.html.
declare const gapi: any;

import {Observable} from "rxjs/Rx";
import {LoggedInUser, IAuthService} from "./iauth.service";

// Add you google client ID here!
const GOOGLE_CLIENT_ID = '';
const LOGGED_IN_USER_KEY = 'user';

export class MockAuthService implements IAuthService {

  protected loggedInUser: LoggedInUser;

  constructor() {
    // Get the user logged in from local storage if applicable
    let userFromStorageData = localStorage.getItem(LOGGED_IN_USER_KEY);
    if (userFromStorageData) {
      try {
        this.loggedInUser = JSON.parse(userFromStorageData);
      } catch (e) {
        // The data was invalid - simply ignore
      }
    }
    // Init Google auth
    this.initGoogleOauth();
  }

  // Check the IAuthService interface for documentation
  signIn(email: string, password: string): Observable<LoggedInUser> {
    return new Observable<LoggedInUser>(subscriber => {
      this.loggedInUser = {
        id: this.createMockId(),
        email: email
      };
      this.setLoggedInUserInStorage();
      subscriber.next(this.loggedInUser);
      subscriber.complete();
    })
  }

  // Check the IAuthService interface for documentation
  getLoggedInUser(): LoggedInUser {
    return this.loggedInUser;
  }

  // Check the IAuthService interface for documentation
  // Also updates in storage for mock purposes.
  changeLoggedInUserPermissionToNormal(): void {
    this.loggedInUser.isAdmin = false;
    this.setLoggedInUserInStorage();
  }

  // Check the IAuthService interface for documentation
  // Also updates in storage for mock purposes.
  changeLoggedInUserPermissionToAdmin(): void {
    this.loggedInUser.isAdmin = true;
    this.setLoggedInUserInStorage();
  }

  // Create a mock id like `mock-id354`
  private createMockId(): string {
    return `mock-id${Math.round(Math.random()*1000)}`;
  }

  // Sets the logged in user in local storage for mock purposes
  private setLoggedInUserInStorage() {
    localStorage.setItem(LOGGED_IN_USER_KEY, JSON.stringify(this.loggedInUser));
  }

  // ------------------------------------------------------------------- Google Authentication
  // These methods sign in with Google but do not communicate with any app server.
  // -------------------------------------------------------------------

  // Check the IAuthService interface for documentation
  signInWithGoogle(): Observable<LoggedInUser> {
    return new Observable<LoggedInUser>(subscriber => {
      gapi.auth2.getAuthInstance().signIn().then((googleUser) => {
        let profile = googleUser.getBasicProfile();
        let googleId = profile.getId(); // Do not send to your backend! Use an ID token instead.
        this.loggedInUser = {
          id: this.createMockId(),
          name: profile.getName(),
          imageUrl: profile.getImageUrl(),
          email: profile.getEmail(),
          google: {
            id: googleId
          }
        }
        this.setLoggedInUserInStorage();
        subscriber.next(this.loggedInUser);
        subscriber.complete();
      }, (e) => {
        // Since this is a mock authorization service, we'll just log the error in the console.
        console.error(e);
      })
    })
  }

  getGoogleClientAuthId() {
    return GOOGLE_CLIENT_ID;
  }

  // Initializes the GoogleAuth object. You must call this method before calling gapi.auth2.GoogleAuth's methods.
  private initGoogleOauth() {
    if (this.getGoogleClientAuthId()) {
      gapi.load('auth2', () => {
        gapi.auth2.init({
          client_id: this.getGoogleClientAuthId()
        });
      });
    }
  }

}
