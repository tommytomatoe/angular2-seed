// By default, the auth service uses the MockAuthService by extending
// from it. When you implement your own auth service, simply remove
// the MockAuthService extension. By default, the seed app uses methods
// declared by the IAuthService interface, which you can rewrite or remove.

import { Injectable } from '@angular/core';
import {MockAuthService} from "./mock-auth.service";
import {IAuthService} from "./iauth.service";

@Injectable()
export class AuthService
  extends MockAuthService /* Remove this to implement your own auth service */
  implements IAuthService /* The default app's auth functionality adheres to this interface */
{

  constructor() {
    /* Remove this if not extending from MockAuthService */
    super();
  }

}
