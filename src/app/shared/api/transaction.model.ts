export interface TransactionModel {
  id?: string,
  passId: number,
  rideId: number,
  amount: string,
  units: number,
  latitude: string,
  longitude: string
}

export class Transaction {
  constructor(
    public model: TransactionModel
  ) { }

  /**
   * Utility function for mapping to tables
   * @returns {[string,string,string,string]}
   */
  static getProperties() {
    return [
      'id',
      'passId',
      'rideId',
      'amount',
      'units',
      'latitude',
      'longitude'
    ]
  }

  getPropertyForDisplay(property: string) {
    if (!this.model) {
      return null;
    }
    return this.model[property];
  }
}
