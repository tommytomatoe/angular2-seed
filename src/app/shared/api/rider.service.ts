import {BaseApiService} from "./base-api";
import {RiderModel, Rider} from "./rider.model";
import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Rx";

@Injectable()
export class RiderService extends BaseApiService<RiderModel, Rider> {

  private resourceUrl = `${this.baseUrl}/riders`;

  constructor(
    protected http: Http
  ) {
    super(http);
  }

  modelToObject(model: RiderModel): Rider {
    return new Rider(model);
  }

  getAll(): Observable<Rider[]> {
    const url = this.resourceUrl;
    return this.baseGetMultiple(url);
  }

  getById(id: number): Observable<Rider> {
    const url = `${this.resourceUrl}/${id}`;
    return this.baseGetOne(url);
  }

  create(riderModel: RiderModel): Observable<Rider> {
    const url = this.resourceUrl;
    return this.baseCreate(url, riderModel);
  }

  update(riderModel: RiderModel): Observable<Rider> {
    const url = `${this.resourceUrl}/${riderModel.id}`;
    return this.baseUpdate(url, riderModel);
  }

  delete(id: number): Observable<string> {
    const url = `${this.resourceUrl}/${id}`;
    return this.baseDelete(url);
  }

  search(name: string): Observable<Rider[]> {
    const url = `${this.resourceUrl}?q=${name}`;
    return this.baseGetMultiple(url);
  }

}
