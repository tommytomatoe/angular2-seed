import {BaseApiService} from "./base-api";
import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Rx";
import {Transaction, TransactionModel} from "./transaction.model";

@Injectable()
export class TransactionService extends BaseApiService<TransactionModel, Transaction> {

  private resourceUrl = `${this.baseUrl}/transactions/`;

  constructor(
    protected http: Http
  ) {
    super(http);
  }

  modelToObject(model: TransactionModel): Transaction {
    return new Transaction(model);
  }

  getAll(): Observable<Transaction[]> {
    const url = this.resourceUrl;
    return this.baseGetMultiple(url);
  }

  getById(id: string): Observable<Transaction> {
    const url = `${this.resourceUrl}/${id}`;
    return this.baseGetOne(url);
  }

  create(transactionModel: TransactionModel): Observable<Transaction> {
    const url = this.resourceUrl;
    return this.baseCreate(url, transactionModel);
  }

  update(transactionModel: TransactionModel): Observable<Transaction> {
    const url = `${this.resourceUrl}/${transactionModel.id}`;
    return this.baseUpdate(url, transactionModel);
  }

  delete(id: number): Observable<string> {
    const url = `${this.resourceUrl}/${id}`;
    return this.baseDelete(url);
  }

}
