import {BaseApiService} from "./base-api";
import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Rx";
import {PassModel, Pass} from "./pass.model";

@Injectable()
export class PassService extends BaseApiService<PassModel, Pass> {

  private resourceUrl = `${this.baseUrl}/passes/`;

  constructor(
    protected http: Http
  ) {
    super(http);
  }

  modelToObject(model: PassModel): Pass {
    return new Pass(model);
  }

  getAll(): Observable<Pass[]> {
    const url = this.resourceUrl;
    return this.baseGetMultiple(url);
  }

  getById(id: number): Observable<Pass> {
    const url = `${this.resourceUrl}/${id}`;
    return this.baseGetOne(url);
  }

  create(passModel: PassModel): Observable<Pass> {
    const url = this.resourceUrl;
    return this.baseCreate(url, passModel);
  }

  update(passModel: PassModel): Observable<Pass> {
    const url = `${this.resourceUrl}/${passModel.id}`;
    return this.baseUpdate(url, passModel);
  }

  delete(id: number): Observable<string> {
    const url = `${this.resourceUrl}/${id}`;
    return this.baseDelete(url);
  }

}
