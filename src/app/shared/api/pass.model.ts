export interface PassModel {
  id?: number,
  riderId: number,
  createdDate: string,
  expirationDate: string
}

export class Pass {
  constructor(
    public model: PassModel
  ) { }

  /**
   * Utility function for mapping to tables
   * @returns {[string,string,string,string]}
   */
  static getProperties() {
    return [
      'id',
      'riderId',
      'createdDate',
      'expirationDate'
    ]
  }

  getPropertyForDisplay(property: string) {
    if (!this.model) {
      return null;
    }
    return this.model[property];
  }
}
