/**
 * Classes that plan to keep their server data with a `model` field on their objects
 * can use this class to make HTTP retrieval easier.
 */

import {Http} from "@angular/http";
import {Observable} from "rxjs/Rx";

export interface BaseApiObjectModel {
  id?: string | number;
}
export interface BaseApiObject<T extends BaseApiObjectModel> {
  model: T;
}

export abstract class BaseApiService<T extends BaseApiObjectModel, U extends BaseApiObject<T>> {

  // If your data server is the same as the one serving your front-end content, just set baseUrl to ''
  protected baseUrl = 'http://localhost:3000';

  abstract modelToObject(model: T): U;

  constructor(
    protected http: Http
  ){}

  protected baseGetOne(url: string): Observable<U> {
    return new Observable<U>((observer) => {
      return this.http.get(url)
        .map((res) => {
          if (res) {
            return res.json();
          }
          return null;
        })
        .subscribe((model: T) => {
          const transformedObject = this.modelToObject(model);
          observer.next(transformedObject);
          observer.complete();
        }, err => observer.error(err));
    });
  }

  protected baseGetMultiple(url: string): Observable<U[]> {
    return new Observable<U[]>((observer) => {
      this.http.get(url)
        .map((res) => {
          if (res) {
            return res.json();
          }
          return null;
        }).subscribe((models) => {
          let transformedObjects = [];
          for (let i = 0; i < models.length; i++) {
            const transformedObject = this.modelToObject(models[i]);
            transformedObjects.push(transformedObject)
          }
          observer.next(transformedObjects);
          observer.complete();
      }, err => observer.error(err));
    });
  }

  protected baseCreate(url: string, model: T): Observable<U> {
    return new Observable<U>((observer) => {
      this.http.post(url, model).map((res) => {
        if (res) {
          return res.json();
        }
        return null;
      }).subscribe((model) => {
        const transformedObject = this.modelToObject(model);
        observer.next(transformedObject);
        observer.complete();
      }, err => observer.error(err));
    });
  }

  protected baseUpdate(url: string, model: T): Observable<U> {
    return new Observable<U>((observer) => {
      this.http.put(url, model).map((res) => {
        if (res) {
          return res.json();
        }
        return null;
      }).subscribe((model) => {
        const transformedObject = this.modelToObject(model);
        observer.next(transformedObject);
        observer.complete();
      }, err => observer.error(err));
    });
  }

  protected baseDelete(url: string): Observable<string> {
    return this.http.delete(url).map((res) => {
      if (res) {
        // Just a status code for delete operations
        return res.text();
      }
      return null;
    });
  }
}
