export interface RiderModel {
  id?: number,
  riderId: number,
  pin: number,
  name: {
    first: string,
    last: string
  },
  email: string,
  phone: string,
  address: string
}

export class Rider {
  constructor(
    public model: RiderModel
  ) { }

  /**
   * Utility function for mapping to tables
   * @returns {[string,string,string,string]}
   */
  static getProperties() {
    return [
      'id',
      'riderId',
      'pin',
      'name',
      'email',
      'phone',
      'address'
    ]
  }

  getPropertyForDisplay(property: string) {
    if (!this.model) {
      return null;
    }
    if (property === 'name') {
      if (!this.model.name) {
        return null;
      }
      return `${this.model.name.first} ${this.model.name.last}`;
    }
    return this.model[property];
  }
}
