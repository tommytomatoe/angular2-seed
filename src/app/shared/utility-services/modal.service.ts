/**
 * A Bootstrap modal service for Angular 2.
 * Most of the code was placed from:  https://ng-bootstrap.github.io/#/home
 * Usage: Make a component that will represent the contents of your modal.
 * Then, call the modal service with that component class:
 * this.routedModal = this.modalService.open(RoutedModalComponent, {backdrop: 'static', keyboard: false, size: 'lg'});
 */

declare const Promise;

import {
  Directive,
  Injector,
  ReflectiveInjector,
  Renderer,
  TemplateRef,
  ViewContainerRef,
  ComponentFactoryResolver,
  ComponentFactory,
  ComponentRef,
  Component,
  EventEmitter,
  ViewRef, NgModule, ModuleWithProviders, OnInit, OnDestroy, AfterViewInit, Input, Output, ElementRef, Injectable
} from '@angular/core';

// Modal Stack.ts

@Injectable()
export class NgbModalStack {
  private modalContainer: NgbModalContainer;

  open(moduleCFR: ComponentFactoryResolver, contentInjector: Injector, content: any, options = {}): NgbModalRef {
    if (!this.modalContainer) {
      throw new Error(
        'Missing modal container, add <template ngbModalContainer></template> to one of your application templates.');
    }

    return this.modalContainer.open(moduleCFR, contentInjector, content, options);
  }

  registerContainer(modalContainer: NgbModalContainer) { this.modalContainer = modalContainer; }
}

@Component({selector: 'ngb-modal-backdrop', template: '', host: {'class': 'modal-backdrop fade in'}})
export class NgbModalBackdrop {
}

export class ContentRef {
  constructor(public nodes: any[], public viewRef?: ViewRef, public componentRef?: ComponentRef<any>) {}
}

// modal-container.ts

@Directive({selector: 'template[ngbModalContainer]'})
export class NgbModalContainer {
  private _backdropFactory: ComponentFactory<NgbModalBackdrop>;
  private _windowFactory: ComponentFactory<NgbModalWindow>;

  constructor(
    private _injector: Injector, private _renderer: Renderer, private _viewContainerRef: ViewContainerRef,
    private _componentFactoryResolver: ComponentFactoryResolver, ngbModalStack: NgbModalStack) {
    this._backdropFactory = _componentFactoryResolver.resolveComponentFactory(NgbModalBackdrop);
    this._windowFactory = _componentFactoryResolver.resolveComponentFactory(NgbModalWindow);

    ngbModalStack.registerContainer(this);
  }

  open(moduleCFR: ComponentFactoryResolver, contentInjector: Injector, content: string | TemplateRef<any>, options):
  NgbModalRef {
    const activeModal = new NgbActiveModal();
    const contentRef = this._getContentRef(moduleCFR, contentInjector, content, activeModal);
    let windowCmptRef: ComponentRef<NgbModalWindow>;
    let backdropCmptRef: ComponentRef<NgbModalBackdrop>;
    let ngbModalRef: NgbModalRef;

    if (options.backdrop !== false) {
      backdropCmptRef = this._viewContainerRef.createComponent(this._backdropFactory, 0, this._injector);
    }

    windowCmptRef = this._viewContainerRef.createComponent(
      this._windowFactory, this._viewContainerRef.length - 1, this._injector, contentRef.nodes);

    ngbModalRef = new NgbModalRef(this._viewContainerRef, windowCmptRef, contentRef, backdropCmptRef);

    activeModal.close = (result: any) => { ngbModalRef.close(result); };
    activeModal.dismiss = (reason: any) => { ngbModalRef.dismiss(reason); };

    this._applyWindowOptions(windowCmptRef.instance, options);

    return ngbModalRef;
  }

  private _applyWindowOptions(windowInstance: NgbModalWindow, options: Object): void {
    ['backdrop', 'keyboard', 'size', 'windowClass'].forEach((optionName: string) => {
      if ((options[optionName]) !== undefined) {
        windowInstance[optionName] = options[optionName];
      }
    });
  }

  private _getContentRef(
    moduleCFR: ComponentFactoryResolver, contentInjector: Injector, content: any,
    context: NgbActiveModal): ContentRef {
    if (!content) {
      return new ContentRef([]);
    } else if (content instanceof TemplateRef) {
      const viewRef = this._viewContainerRef.createEmbeddedView(<TemplateRef<NgbActiveModal>>content, context);
      return new ContentRef([viewRef.rootNodes], viewRef);
    } else if (typeof content === 'string') {
      return new ContentRef([[this._renderer.createText(null, `${content}`)]]);
    } else {
      const contentCmptFactory = moduleCFR.resolveComponentFactory(content);
      const modalContentInjector =
        ReflectiveInjector.resolveAndCreate([{provide: NgbActiveModal, useValue: context}], contentInjector);
      const componentRef = this._viewContainerRef.createComponent(contentCmptFactory, 0, modalContentInjector);
      return new ContentRef([[componentRef.location.nativeElement]], componentRef.hostView, componentRef);
    }
  }
}

export enum ModalDismissReasons {
  BACKDROP_CLICK,
  ESC
}

// Modal-Ref.ts

/**
 * A reference to an active (currently opened) modal. Instances of this class
 * can be injected into components passed as modal content.
 */
@Injectable()
export class NgbActiveModal {
  /**
   * Can be used to close a modal, passing an optional result.
   */
  close(result?: any): void {}

  /**
   * Can be used to dismiss a modal, passing an optional reason.
   */
  dismiss(reason?: any): void {}
}

/**
 * A reference to a newly opened modal.
 */
@Injectable()
export class NgbModalRef {
  private _resolve: (result?: any) => void;
  private _reject: (reason?: any) => void;

  /**
   * The instance of component used as modal's content.
   * Undefined when a TemplateRef is used as modal's content.
   */
  get componentInstance(): any {
    if (this._contentRef.componentRef) {
      return this._contentRef.componentRef.instance;
    }
  }

  // only needed to keep TS1.8 compatibility
  set componentInstance(instance: any) {}

  /**
   * A promise that is resolved when a modal is closed and rejected when a modal is dismissed.
   */
  result: any;

  constructor(
    private _viewContainerRef: ViewContainerRef, private _windowCmptRef: ComponentRef<NgbModalWindow>,
    private _contentRef: ContentRef, private _backdropCmptRef?: ComponentRef<NgbModalBackdrop>) {
    _windowCmptRef.instance.dismissEvent.subscribe((reason: any) => { this.dismiss(reason); });

    this.result = new Promise((resolve, reject) => {
      this._resolve = resolve;
      this._reject = reject;
    });
    this.result.then(null, () => {});
  }

  /**
   * Can be used to close a modal, passing an optional result.
   */
  close(result?: any): void {
    if (this._windowCmptRef) {
      this._resolve(result);
      this._removeModalElements();
    }
  }

  /**
   * Can be used to dismiss a modal, passing an optional reason.
   */
  dismiss(reason?: any): void {
    if (this._windowCmptRef) {
      this._reject(reason);
      this._removeModalElements();
    }
  }

  private _removeModalElements() {
    this._viewContainerRef.remove(this._viewContainerRef.indexOf(this._windowCmptRef.hostView));
    if (this._backdropCmptRef) {
      this._viewContainerRef.remove(this._viewContainerRef.indexOf(this._backdropCmptRef.hostView));
    }
    if (this._contentRef && this._contentRef.viewRef) {
      this._viewContainerRef.remove(this._viewContainerRef.indexOf(this._contentRef.viewRef));
    }

    this._windowCmptRef = null;
    this._backdropCmptRef = null;
    this._contentRef = null;
  }
}


// Modal-window.ts

@Component({
  selector: 'ngb-modal-window',
  host: {
    '[class]': '"modal fade in" + (windowClass ? " " + windowClass : "")',
    'role': 'dialog',
    'tabindex': '-1',
    'style': 'display: block;',
    '(keyup.esc)': 'escKey($event)',
    '(click)': 'backdropClick($event)'
  },
  template: `
    <div [class]="'modal-dialog' + (size ? ' modal-' + size : '')" role="document">
        <div class="modal-content"><ng-content></ng-content></div>
    </div>
    `
})
export class NgbModalWindow implements OnInit,
  AfterViewInit, OnDestroy {
  private _elWithFocus: Element;  // element that is focused prior to modal opening

  @Input() backdrop: boolean | string = true;
  @Input() keyboard = true;
  @Input() size: string;
  @Input() windowClass: string;

  @Output('dismiss') dismissEvent = new EventEmitter();

  constructor(private _elRef: ElementRef, private _renderer: Renderer) {}

  backdropClick($event): void {
    if (this.backdrop === true && this._elRef.nativeElement === $event.target) {
      this.dismiss(ModalDismissReasons.BACKDROP_CLICK);
    }
  }

  escKey($event): void {
    if (this.keyboard && !$event.defaultPrevented) {
      this.dismiss(ModalDismissReasons.ESC);
    }
  }

  dismiss(reason): void { this.dismissEvent.emit(reason); }

  ngOnInit() {
    this._elWithFocus = document.activeElement;
    this._renderer.setElementClass(document.body, 'modal-open', true);
  }

  ngAfterViewInit() {
    if (!this._elRef.nativeElement.contains(document.activeElement)) {
      this._renderer.invokeElementMethod(this._elRef.nativeElement, 'focus', []);
    }
  }

  ngOnDestroy() {
    if (this._elWithFocus && document.body.contains(this._elWithFocus)) {
      this._renderer.invokeElementMethod(this._elWithFocus, 'focus', []);
    } else {
      this._renderer.invokeElementMethod(document.body, 'focus', []);
    }

    this._elWithFocus = null;
    this._renderer.setElementClass(document.body, 'modal-open', false);
  }
}

// Modal.ts

/**
 * Represent options available when opening new modal windows.
 */
export interface NgbModalOptions {
  /**
   * Whether a backdrop element should be created for a given modal (true by default).
   * Alternatively, specify 'static' for a backdrop which doesn't close the modal on click.
   */
  backdrop?: boolean | 'static';

  /**
   * Whether to close the modal when escape key is pressed (true by default).
   */
  keyboard?: boolean;

  /**
   * Size of a new modal window.
   */
  size?: 'sm' | 'lg';

  /**
   * Custom class to append to the modal window
   */
  windowClass?: string;
}

export enum ConfirmComponentResult {
  Yes,
  No,
  Dismiss
}

@Component({
    template: `
    <div class="modal-header">
      <button type="button" class="close" aria-label="Close" (click)="activeModal.close(ConfirmComponentResult.Dismiss)">
        <span aria-hidden="true">&times;</span>
      </button>
      <h4 class="modal-title">{{title}}</h4>
    </div>
    <div class="modal-body">
      <p>{{message}}</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" (click)="activeModal.close(ConfirmComponentResult.Yes)">{{yesButtonText}}</button>
      <button type="button" class="btn btn-secondary" (click)="activeModal.close(ConfirmComponentResult.No)">{{noButtonText}}</button>
    </div>
    `
})
export class ConfirmationModalComponent {
  private ConfirmComponentResult = ConfirmComponentResult;
  title = '';
  message = '';
  yesButtonText = 'Yes';
  noButtonText = 'No';
  constructor(
    public activeModal: NgbActiveModal
  ) {}
}

/**
 * A service to open modal windows. Creating a modal is straightforward: create a template and pass it as an argument to
 * the "open" method!
 */
@Injectable()
export class NgbModal {

  constructor(
    private _moduleCFR: ComponentFactoryResolver, private _injector: Injector, private _modalStack: NgbModalStack) {}

  /**
   * Opens a new modal window with the specified content and using supplied options. Content can be provided
   * as a TemplateRef or a component type. If you pass a component type as content than instances of those
   * components can be injected with an instance of the NgbActiveModal class. You can use methods on the
   * NgbActiveModal class to close / dismiss modals from "inside" of a component.
   */
  open(content: any, options: NgbModalOptions = {}): NgbModalRef {
    return this._modalStack.open(this._moduleCFR, this._injector, content, options);
  }

  confirm(details: {title?: string, required?: boolean, yesButtonText?: string, noButtonText?: string, message: string}) {
    let options: NgbModalOptions = { size: 'sm' };
    if (details.required) {
      options.keyboard = false;
      options.backdrop = 'static';
    }
    const confirmationDialog = this._modalStack.open(this._moduleCFR, this._injector, ConfirmationModalComponent, options);
    confirmationDialog.componentInstance.title = details.title;
    confirmationDialog.componentInstance.message = details.message;
    if (details.yesButtonText) {
      confirmationDialog.componentInstance.yesButtonText = details.yesButtonText;
    }
    if (details.noButtonText) {
      confirmationDialog.componentInstance.noButtonText = details.noButtonText;
    }
    return confirmationDialog;
  }
}

@NgModule({
  declarations: [NgbModalContainer, NgbModalBackdrop, NgbModalWindow, ConfirmationModalComponent],
  entryComponents: [NgbModalBackdrop, NgbModalWindow, ConfirmationModalComponent],
  providers: [NgbModal],
  exports: [NgbModalContainer]
})
export class NgbModalModule {
  static forRoot(): ModuleWithProviders { return {ngModule: NgbModalModule, providers: [NgbModal, NgbModalStack]}; }
}

