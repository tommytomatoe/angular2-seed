/**
 * A very simple error service that displays errors with toasts.
 */

import {Injectable} from "@angular/core";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Injectable()
export class ErrorService {

  constructor(
    public toastr: ToastsManager
  ) {}

  unknown(err: any) {
    this.message('Sorry, an unknown error has occurred');
    console.error(err);
  }

  message(message: string) {
    this.toastr.error('Error', message);
  }

  serverUnreachable() {
    this.message('The mock server was unreachable - is it running? Check out the mock-server README.md.');
  }

}
