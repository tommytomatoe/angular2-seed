/**
 * A service that provides common text masks to be used with the angular2-text-mask NPM package.
 * Much of this code was simply placed here from:
 * https://github.com/text-mask/text-mask/tree/master/addons/
 */

import {Injectable} from "@angular/core";

const dollarSign = '$'
const emptyString = ''
const comma = ','
const period = '.'
const minus = '-'
const minusRegExp = /-/
const nonDigitsRegExp = /\D+/g
const number = 'number'
const digitRegExp = /\d/
const caretTrap = '[]'
const asterisk = '*'
const dot = '.'
const atSymbol = '@'
const space = ' '
const g = 'g'
const anyNonWhitespaceRegExp = /[^\s]/
const anyNonDotOrWhitespaceRegExp = /[^.\s]/
const allWhitespaceRegExp = /\s/g
const allAtSymbolsRegExp = /@/g
const atDot = '@.'
const dotDot = '..'
const emptyArray = []
const allDotsRegExp = /\./g

@Injectable()
export class MaskService {

  constructor(

  ) {}

  createAutoCorrectedDatePipe(dateFormat?, minDate?, maxDate?) {
    return function(conformedValue): any {
      const indexesOfPipedChars = []
      dateFormat = dateFormat || 'mm dd yyyy';
      const dateFormatArray = dateFormat.split(/[^dmy]+/)
      const maxValue = maxDate || {'dd': 31, 'mm': 12, 'yyyy': 9999}
      const minValue = minDate || {'dd': 1, 'mm': 1, 'yyyy': 1}
      const conformedValueArr = conformedValue.split('')

      // Check first digit
      dateFormatArray.forEach((format) => {
        const position = dateFormat.indexOf(format)
        const maxFirstDigit = parseInt(maxValue[format].toString().substr(0, 1), 10)

        if (parseInt(conformedValueArr[position], 10) > maxFirstDigit) {
          conformedValueArr[position + 1] = conformedValueArr[position]
          conformedValueArr[position] = 0
          indexesOfPipedChars.push(position)
        }
      })

      // Check for invalid date
      const isInvalid = dateFormatArray.some((format) => {
        const position = dateFormat.indexOf(format)
        const length = format.length
        const textValue = conformedValue.substr(position, length).replace(/\D/g, '')
        const value = parseInt(textValue, 10)

        return value > maxValue[format] || (textValue.length === length && value < minValue[format])
      })

      if (isInvalid) {
        return false
      }

      return {
        value: conformedValueArr.join(''),
        indexesOfPipedChars
      }
    }
  }

  createNumberMask({
    prefix = dollarSign,
    suffix = emptyString,
    includeThousandsSeparator = true,
    thousandsSeparatorSymbol = comma,
    allowDecimal = false,
    decimalSymbol = period,
    decimalLimit = 2,
    requireDecimal = false,
    allowNegative = false,
  } = {}) {
    const prefixLength = prefix.length

    let numberMask = (rawValue) => {
      const rawValueLength = rawValue.length

      if (
        rawValue === emptyString ||
        (rawValue[0] === prefix[0] && rawValueLength === 1)
      ) {
        return <any>prefix.split(emptyString).concat(<any>[digitRegExp]).concat(suffix.split(emptyString))
      }

      const indexOfLastDecimal = rawValue.lastIndexOf(decimalSymbol)
      const hasDecimal = indexOfLastDecimal !== -1
      const isNegative = (rawValue[0] === minus) && allowNegative

      let integer
      let fraction
      let mask

      if (hasDecimal && (allowDecimal || requireDecimal)) {
        integer = rawValue.slice(0, indexOfLastDecimal)

        fraction = rawValue.slice(indexOfLastDecimal + 1, rawValueLength)
        fraction = this.convertToMask(fraction.replace(nonDigitsRegExp, emptyString))
      } else {
        integer = rawValue
      }

      integer = integer.replace(nonDigitsRegExp, emptyString)

      integer = (includeThousandsSeparator) ? this.addThousandsSeparator(integer, thousandsSeparatorSymbol) : integer

      mask = this.convertToMask(integer)

      if ((hasDecimal && allowDecimal) || requireDecimal === true) {
        if (rawValue[indexOfLastDecimal - 1] !== decimalSymbol) {
          mask.push(caretTrap)
        }

        mask.push(decimalSymbol, caretTrap)

        if (fraction) {
          if (typeof decimalLimit === number) {
            fraction = fraction.slice(0, decimalLimit)
          }

          mask = mask.concat(fraction)
        }

        if (requireDecimal === true && rawValue[indexOfLastDecimal - 1] === decimalSymbol) {
          mask.push(digitRegExp)
        }
      }

      if (prefixLength > 0) {
        mask = prefix.split(emptyString).concat(mask)
      }

      if (isNegative) {
        // If user is entering a negative number, add a mask placeholder spot to attract the caret to it.
        if (mask.length === prefixLength) {
          mask.push(digitRegExp)
        }

        mask = [minusRegExp].concat(mask)
      }

      if (suffix.length > 0) {
        mask = mask.concat(suffix.split(emptyString))
      }

      return mask
    }

    return numberMask;
  }

  emailMask(rawValue, config) {
    rawValue = rawValue.replace(allWhitespaceRegExp, emptyString)

    const {placeholderChar, currentCaretPosition} = config
    const indexOfFirstAtSymbol = rawValue.indexOf(atSymbol)
    const indexOfLastDot = rawValue.lastIndexOf(dot)
    const indexOfTopLevelDomainDot = (indexOfLastDot < indexOfFirstAtSymbol) ? -1 : indexOfLastDot

    let localPartToDomainConnector = this.getConnector(rawValue, indexOfFirstAtSymbol + 1, atSymbol)
    let domainNameToTopLevelDomainConnector = this.getConnector(rawValue, indexOfTopLevelDomainDot - 1, dot)

    let localPart = this.getLocalPart(rawValue, indexOfFirstAtSymbol)
    let domainName = this.getDomainName(rawValue, indexOfFirstAtSymbol, indexOfTopLevelDomainDot, placeholderChar)
    let topLevelDomain = this.getTopLevelDomain(rawValue, indexOfTopLevelDomainDot, placeholderChar, currentCaretPosition)

    localPart = this.convertToEmailMask(localPart)
    domainName = this.convertToEmailMask(domainName)
    topLevelDomain = this.convertToEmailMask(topLevelDomain, true)

    const mask = localPart
      .concat(localPartToDomainConnector)
      .concat(domainName)
      .concat(domainNameToTopLevelDomainConnector)
      .concat(topLevelDomain)

    return mask
  }

  emailPipe(conformedValue, config) {
    const {currentCaretPosition, rawValue, previousConformedValue, placeholderChar} = config

    let value = conformedValue

    value = this.removeAllAtSymbolsButFirst(value)

    const indexOfAtDot = value.indexOf(atDot)

    const emptyEmail = rawValue.match(new RegExp(`[^@\\s.${placeholderChar}]`)) === null

    if (emptyEmail) {
      return emptyString
    }

    if (
      value.indexOf(dotDot) !== -1 ||
      indexOfAtDot !== -1 && currentCaretPosition !== (indexOfAtDot + 1) ||
      rawValue.indexOf(atSymbol) === -1 && previousConformedValue !== emptyString && rawValue.indexOf(dot) !== -1
    ) {
      return false
    }

    const indexOfAtSymbol = value.indexOf(atSymbol)
    const domainPart = value.slice(indexOfAtSymbol + 1, value.length)

    if (
      (domainPart.match(allDotsRegExp) || emptyArray).length > 1 &&
      value.substr(-1) === dot &&
      currentCaretPosition !== rawValue.length
    ) {
      value = value.slice(0, value.length - 1)
    }

    return value;
  }

  createEmailMask() {
    return this.emailMask.bind(this);
  }

  createEmailPipe() {
    return this.emailPipe.bind(this);
  }

  createTwentyFourHourTimePipe() {
    return (value) => {
      let transformedValue = value;
      if (parseInt(value[0]) > 2) {
        transformedValue = `0${value[0]}${value.substring(2)}`;
      }
      return transformedValue;
    }
  }

  // ------------------------------------------------------------------- Utility

  private removeAllAtSymbolsButFirst(str) {
    let atSymbolCount = 0

    return str.replace(allAtSymbolsRegExp, () => {
      atSymbolCount++

      return (atSymbolCount === 1) ? atSymbol : emptyString
    })
  }

  private convertToMask(strNumber) {
    return strNumber
      .split(emptyString)
      .map((char) => digitRegExp.test(char) ? digitRegExp : char)
  }

  private addThousandsSeparator(n, thousandsSeparatorSymbol) {
    return n.replace(/\B(?=(\d{3})+(?!\d))/g, thousandsSeparatorSymbol)
  }

  private getConnector(rawValue, indexOfConnection, connectionSymbol) {
    const connector = []

    if (rawValue[indexOfConnection] === connectionSymbol) {
      connector.push(connectionSymbol)
    } else {
      connector.push(caretTrap, connectionSymbol)
    }

    connector.push(caretTrap)

    return connector
  }

  private getLocalPart(rawValue, indexOfFirstAtSymbol) {
    if (indexOfFirstAtSymbol === -1) {
      return rawValue
    } else {
      return rawValue.slice(0, indexOfFirstAtSymbol)
    }
  }

  private getDomainName(rawValue, indexOfFirstAtSymbol, indexOfTopLevelDomainDot, placeholderChar) {
    let domainName = emptyString

    if (indexOfFirstAtSymbol !== -1) {
      if (indexOfTopLevelDomainDot === -1) {
        domainName = rawValue.slice(indexOfFirstAtSymbol + 1, rawValue.length)
      } else {
        domainName = rawValue.slice(indexOfFirstAtSymbol + 1, indexOfTopLevelDomainDot)
      }
    }

    domainName = domainName.replace(new RegExp(`[\\s${placeholderChar}]`, g), emptyString)

    if (domainName === atSymbol) {
      return asterisk
    } else if (domainName.length < 1) {
      return space
    } else if (domainName[domainName.length - 1] === dot) {
      return domainName.slice(0, domainName.length - 1)
    } else {
      return domainName
    }
  }

  private getTopLevelDomain(rawValue, indexOfTopLevelDomainDot, placeholderChar, currentCaretPosition) {
    let topLevelDomain = emptyString

    if (indexOfTopLevelDomainDot !== -1) {
      topLevelDomain = rawValue.slice(indexOfTopLevelDomainDot + 1, rawValue.length)
    }

    topLevelDomain = topLevelDomain.replace(new RegExp(`[\\s${placeholderChar}.]`, g), emptyString)

    if (topLevelDomain.length === 0) {
      return (rawValue[indexOfTopLevelDomainDot - 1] === dot && currentCaretPosition !== rawValue.length) ?
        asterisk :
        emptyString
    } else {
      return topLevelDomain
    }
  }

  private convertToEmailMask(str, noDots?) {
    return str
      .split(emptyString)
      .map((char) => char === space ? char : (noDots) ? anyNonDotOrWhitespaceRegExp : anyNonWhitespaceRegExp)
  }

}
