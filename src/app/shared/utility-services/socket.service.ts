/**
 * A simple wrapper for a socket - by default we use socket.io.
 * Use listenForEvent for a specific socket event with a callback function.
 * Use stopListeningForEvent (for example, in ngOnDestroy) to prevent memory leaks
 * and duplicate functionality.
 */

import {Injectable, EventEmitter} from "@angular/core";

declare const io;

Injectable()
export class SocketService {

  private socket: any;

  constructor() {
    this.socket = io('http://localhost:3001');
  }

  listenForEvent(event: string, callback: Function) {
    this.socket.on(event, (data) => {
      callback(data);
    });
  }

  stopListeningForEvent(event: string) {
    this.socket.removeAllListeners(event);
  }

}
