/**
 * A module for making elements draggable and droppable by zones.
 * Designate an item as draggable with [appDraggable]="myModel", where the model is some object you want to deal with on drop.
 * You can give an appDraggable element a dropzone identifier - then the user can only drop in zones with that identifier.
 * Designate an item as droppable with [appDroppable]="'myZone'", where myZone is the identifier for that zone.
 * The droppable element has an onDrop emitter that you can use.
 * Example usage for a draggable item:
 * <li class="draggable-li" [appDraggable]="item" [dropzone]="'zone2'" *ngFor="let item of zone1Items">{{item}}</li>
 * Example usage for a droppable zone:
 * <div class="card-block" [appDroppable]="'passZone'" (onDrop)="onTransactionDrop($event)">
 */

import {
  Directive, ModuleWithProviders, NgModule, Injectable, Input, ElementRef, Renderer, Output, EventEmitter,
  NgZone
} from "@angular/core";

const CSS = {
  dragover: 'app-dragover'
}

interface CurrentlyDragging {
  data: any
  dropzone: string
  currentlyOver?: CtsAppDroppableDirective
}

@Injectable()
export class CtsAppDragDropService {
  currentlyDragging: CurrentlyDragging = null;

  endDrag() {
    if (this.currentlyDragging) {
      if (this.currentlyDragging.currentlyOver) {
        if (this.currentlyDragging.currentlyOver.onDrop) {
          this.currentlyDragging.currentlyOver.onDrop.emit(this.currentlyDragging.data);
        }
      }
    }
    this.currentlyDragging = null;
  }
}

@Directive({
  selector: '[appDraggable]',
  host: {
    '[attr.draggable]': 'true'
  }
})
export class CtsAppDraggableDirective {

  // The model to be passed on drop
  @Input('appDraggable')
  appDraggable: any;

  // Where the element can be dropped - null for any droppable
  @Input('dropzone')
  dropzone: string = null;

  constructor(
    private el: ElementRef,
    private render: Renderer,
    private service: CtsAppDragDropService,
    private ngZone: NgZone
  ) {}

  ngAfterViewInit() {
    this.ngZone.runOutsideAngular(() => {
      let nativeElement = this.el.nativeElement;
      nativeElement.addEventListener('dragstart', (e) => {
        //e.preventDefault();
        this.service.currentlyDragging = {
          data: this.appDraggable,
          dropzone: this.dropzone
        }
      });
      nativeElement.addEventListener('dragend', (e) => {
        this.ngZone.run(() => {
          //e.preventDefault();
          removeClassFromAllElements(CSS.dragover);
          this.service.endDrag();
        });
      });
    });
  }

}

@Directive({
  selector: '[appDroppable]'
})
export class CtsAppDroppableDirective {
  // The name of the dropzone - can be null
  @Input('appDroppable')
  appDroppable: string = null;

  @Output('onDrop')
  onDrop = new EventEmitter();

  constructor(private el: ElementRef,
              private render: Renderer,
              private service: CtsAppDragDropService,
              private ngZone: NgZone) {
  }

  ngAfterViewInit() {
    this.ngZone.runOutsideAngular(() => {
      let nativeElement = this.el.nativeElement;
      nativeElement.addEventListener('dragenter', (e) => {
        e.preventDefault();
      });
      nativeElement.addEventListener('dragleave', (e) => {
        e.preventDefault();
        nativeElement.classList.remove(CSS.dragover);
        this.service.currentlyDragging.currentlyOver = null;
      });
      nativeElement.addEventListener('dragover', (e) => {
        e.preventDefault();
        if (this.service.currentlyDragging) {
          if (!this.service.currentlyDragging.dropzone || this.service.currentlyDragging.dropzone === this.appDroppable) {
            nativeElement.classList.add(CSS.dragover);
            this.service.currentlyDragging.currentlyOver = this;
          }
        }
      });
    });
  }
}

@NgModule({
  declarations: [CtsAppDraggableDirective, CtsAppDroppableDirective],
  exports: [CtsAppDraggableDirective, CtsAppDroppableDirective],
  providers: [CtsAppDragDropService]
})
export class CtsAppDragDropModule {
  static forRoot(): ModuleWithProviders { return {ngModule: CtsAppDragDropModule, providers: []}; }
}

function removeClassFromAllElements(className: string) {
  let elements = document.getElementsByClassName(className);
  if (elements && elements.length) {
    while (elements.length) {
      elements[0].classList.remove(className);
    }
  }
}
