/**
 * A utility Angular 2 wrapper for Bootstrap Tour: http://bootstraptour.com/
 * Use as a directive on elements like so:
 * <div [appTour]="'Welcome'" [tourContent]="'Welcome! This is an example feature tour.'" [tourPlacement]="'bottom'" [tourIndex]="0">
 * Then use bootstrapTourService.start();
 */

import {Injectable, Directive, Input, ElementRef, NgModule, ModuleWithProviders, NgZone} from "@angular/core";

declare const Tour;

export interface TourStep {
  element: string
  title: string
  backdrop: boolean
  content: string
  path?: string
  placement?: string
}

@Injectable()
export class BootstrapTourService {
  private tour;
  private steps: any[] = [];

  constructor(
  ) {
    if (typeof Tour !== 'undefined') {
      this.tour = new Tour();
    }
  }

  addStep(step: TourStep, index?: number) {
    if (index !== undefined && index !== null) {
      this.steps.splice(index, 0, step);
    } else {
      this.steps.push(step);
    }
  }

  start() {
    this.tour.addSteps(this.steps);
    this.tour.init(true);
    this.tour.start(true);
  }
}

@Directive({
  selector: '[appTour]',
  host: {
    '[attr.appTourId]': 'appTourId'
  }
})
export class BootstrapTourDirective {

  @Input('appTour')
  appTour = ''; // Title

  @Input('tourContent')
  tourContent = '';

  @Input('tourIndex')
  tourIndex: number = null;

  @Input('tourPlacement')
  tourPlacement = 'right';

  appTourId = 'apptour-' + Math.round(Math.random() * 1000000);

  constructor(
    private el: ElementRef,
    private bootstrapTourService: BootstrapTourService
  ) { }

  ngAfterViewInit() {
    this.bootstrapTourService.addStep({
      backdrop: true,
      element: `[appTourId="${this.appTourId}"]`,
      title: this.appTour,
      content: this.tourContent,
      placement: this.tourPlacement
    }, this.tourIndex);
  }
}

@NgModule({
  declarations: [BootstrapTourDirective],
  exports: [BootstrapTourDirective],
  providers: [BootstrapTourService]
})
export class BootstrapTourModule {
  static forRoot(): ModuleWithProviders { return {ngModule: BootstrapTourModule, providers: [BootstrapTourService]}; }
}

