/**
 * This module can be used to easily create a Wizard in Angular 2.
 * Usage: include the WizardModule component in your app module.
 * In your component, use markup similar to:
 * <div ctsappWizard (onNext)="func($event)">
 *     <div ctsappWizardSteps></div>
 *      <div [ctsappWizardStep]="'My First Step'">
 *          This is the first step.
 *      </div>
 *      <div [ctsappWizardStep]="'My Second Step'">
 *          This is the second step.
 *      </div>
 *     <button ctsappWizardPrevious>Previous</button>
 *     <button ctsappWizardNext>Next</button>
 * </div>
 *
 * The ctsappWizardSteps directive will automatically grab all steps inside the
 * master ctsappWizard div and insert the titles for you.
 * The ctsappWizardPrevious and ctsappWizardNext buttons control the flow.
 */

import {
  Directive, ElementRef, AfterViewInit, NgModule, ModuleWithProviders, Component,
  Injectable, Input, EventEmitter, Output
} from "@angular/core";
import {CommonModule} from "@angular/common";

interface IWizardStep {
  text: string
}

interface IWizard {
  id: string
  el: HTMLElement
  steps: IWizardStep[]
  currentIndex: number
  directive?: WizardDirective
}

/**
 * This service is used internally for the wizard directives.
 */
@Injectable()
export class WizardService {

  wizards: {[id: string]: IWizard} = {};

  constructor() {}

  getOrSetWizard(el: HTMLElement) {
    let wizardId = el.attributes['wizardId'];
    if (!el.attributes['wizardId']) {
      wizardId = 'wizard-' + Math.round(Math.random() * 100000);
      this.wizards[wizardId] = {
        id: wizardId,
        el: el,
        steps: [],
        currentIndex: 0
      };
      el.attributes['wizardId'] = wizardId;
    }
    return this.wizards[wizardId];
  }

  unregisterWizard(wizardId: string) {
    delete this.wizards[wizardId];
  }

  setIsAtLast(wizard: IWizard, isAtLast: boolean) {
    wizard.directive.isAtLast = isAtLast;
  }

  onFinish(wizard: IWizard) {
    wizard.directive.onFinish.emit(null);
  }
  onNext(wizard: IWizard, index: number) {
    wizard.directive.onNext.emit(index);
  }
  onPrevious(wizard: IWizard, index: number) {
    wizard.directive.onPrevious.emit(index);
  }

}

/**
 * The master ctsappWizard directive should go on a div that wraps the other wizard
 * directives. Available output functions can be used for events.
 */
@Directive({
  selector: '[ctsappWizard]',
  exportAs:'wizard'
})
export class WizardDirective implements AfterViewInit {

  /**
   * Provide a function to emit when the last step has been reached and "next" is pressed.
   * @type {EventEmitter}
     */
  @Output('onFinish')
  onFinish = new EventEmitter();

  /**
   * Provide a function to tell when the next button is pressed. The function is given
   * an index.
   * @type {EventEmitter}
     */
  @Output('onNext')
  onNext = new EventEmitter();

  /**
   * Provide a function to tell when the previous button is pressed. The function is given
   * an index.
   * @type {EventEmitter}
   */
  @Output('onPrevious')
  onPrevious = new EventEmitter();

  isAtLast = false;

  constructor(
    private el: ElementRef,
    private wizardService: WizardService
  ) {}

  ngAfterViewInit() {
    //this.el.nativeElement.style.background = 'red';

    // Find all wizard steps
    // apply an NgSwitch to them
    let wizard = this.wizardService.getOrSetWizard(this.el.nativeElement);
    wizard.directive = this;
  }

  ngOnDestroy() {
    this.wizardService.unregisterWizard(this.el.nativeElement.attributes['wizardId']);
  }

}

/**
 * This ctsappWizardSteps component can be placed anywhere inside a div that has
 * the ctsappWizard directive.
 * This component grabs the steps that are also inside the master div and generates
 * title buttons for you.
 */
@Component({
  selector: '[ctsappWizardSteps]',
  template: `
    <div class="steps">
      <div class="step-wrapper" 
      [ngClass]="{'active': wizard?.currentIndex == i, 'checked': wizard?.currentIndex > i, 'future': wizard?.currentIndex < i}" 
      *ngFor="let step of steps; let i = index">
        <div class="step-icon">
          <i [hidden]="wizard?.currentIndex <= i" class="icon ion-checkmark"></i>
          <span [hidden]="wizard?.currentIndex > i">{{i}}</span>
        </div>
        <div class="step-title">{{step.text}}</div>
      </div>
    </div>
  `,
  styles: [
    `
      .steps {
        display: flex;
        border-bottom: 1px solid rgba(0,0,0,0.1);
      }
      .step-wrapper {
        padding: 0.5rem 1rem;
        display: flex;
        align-items: baseline;
      }
      .step-wrapper.checked {
        border-bottom: 2px solid green;
        background: rgba(0, 0, 0, 0.05);
      }
      .step-wrapper.checked .step-icon {
        border: 2px solid green;
        color: green;
      }
      .step-wrapper.active {
        border-bottom: 2px solid #3A80FF;
      }
      .step-wrapper.active .step-icon {
        border: 2px solid #3A80FF;
        color: #3A80FF;
      }
      .step-wrapper.future {
        background: rgba(0, 0, 0, 0.05);
      }
      .step-wrapper.future .step-icon {
        opacity: 0.3;
      }
      .step-wrapper.future .step-title {
        opacity: 0.3;
      }
      .step-wrapper .step-icon {
        border: 2px solid gray;
        color: gray;
        border-radius: 50%;
        height: 2rem;
        width: 2rem;
        display: flex;
        align-items: center;
        justify-content: center;
      }
      .step-wrapper .step-icon span {
        line-height: 1rem;
      }
      .step-wrapper .step-title {
        padding-left: 0.5rem;
      }
    `
  ]
})
export class WizardStepsComponent implements AfterViewInit {

  wizard: IWizard;
  steps: IWizardStep[] = [];

  constructor(
    private el: ElementRef,
    private wizardService: WizardService
  ) {}

  ngAfterViewInit() {
    let wizard = this.wizardService.getOrSetWizard(this.el.nativeElement.parentNode);
    this.wizard = wizard;
    this.steps = wizard.steps;
  }

}

/**
 * The step directive wraps the content of any one of your steps.
 */
@Directive({
  selector: '[ctsappWizardStep]',
  host: {
    '[hidden]': 'wizard?.currentIndex != this.index'
  }
})
export class WizardStepDirective implements AfterViewInit {

  /**
   * The title to display on the button.
   * @type {string}
     */
  @Input('ctsappWizardStep')
  ctsappWizardStep = '';

  wizard: IWizard;
  index = 0;

  constructor(
    private el: ElementRef,
    private wizardService: WizardService
  ) {

  }

  ngAfterViewInit() {
    setTimeout(() => {
      let wizard = this.wizardService.getOrSetWizard(this.el.nativeElement.parentNode);
      this.index = wizard.steps.length;
      wizard.steps.push({
        text: this.ctsappWizardStep
      });
      this.wizard = wizard;
    }, 10)
  }

}

/**
 * The previous button for the wizard should have this directive. It can be enabled
 * or disabled via an [enabled] input.
 */
@Directive({
  selector: '[ctsappWizardPrevious]',
  host: {
    '(click)': 'onClick()',
    '[style.opacity]': 'enabled && wizard?.currentIndex > 0 ? 1 : 0.5',
    '[style.cursor]': 'enabled && wizard?.currentIndex > 0 ? "pointer" : "default"',
  }
})
export class WizardPreviousButtonDirective implements AfterViewInit {

  /**
   * Whether the previous button will be enabled.
   * @type {boolean}
     */
  @Input('enabled')
  enabled: boolean = true;

  wizard: IWizard;

  constructor(
    private el: ElementRef,
    private wizardService: WizardService
  ) {

  }

  ngAfterViewInit() {
    let wizardEl = getClosest(this.el.nativeElement, '[ctsappWizard]');
    let wizard = this.wizardService.getOrSetWizard(wizardEl);
    this.wizard = wizard;
  }

  onClick() {
    if (this.enabled) {
      if (this.wizard.currentIndex > 0) {
        this.wizard.currentIndex -= 1;
        this.wizardService.onPrevious(this.wizard, this.wizard.currentIndex);
        this.wizardService.setIsAtLast(this.wizard, false);
      }
    }
  }

}

/**
 * The next button for the wizard should have this directive. It can be enabled
 * or disabled via an [enabled] input.
 */
@Directive({
  selector: '[ctsappWizardNext]',
  host: {
    '(click)': 'onClick()',
    '[style.opacity]': 'enabled ? 1 : 0.5',
    '[style.cursor]': 'enabled ? "pointer" : "default"',
  }
})
export class WizardNextButtonDirective implements AfterViewInit {

  /**
   * Whether the next button will be enabled.
   * @type {boolean}
   */
  @Input('enabled')
  enabled: boolean = true;

  wizard: IWizard;

  constructor(
    private el: ElementRef,
    private wizardService: WizardService
  ) {

  }

  ngAfterViewInit() {
    let wizardEl = getClosest(this.el.nativeElement, '[ctsappWizard]');
    let wizard = this.wizardService.getOrSetWizard(wizardEl);
    this.wizard = wizard;
  }

  onClick() {
    if (this.enabled) {
      if (this.wizard.currentIndex < this.wizard.steps.length - 1) {
        this.wizard.currentIndex += 1;
        this.wizardService.onNext(this.wizard, this.wizard.currentIndex);
        if (this.wizard.currentIndex >= this.wizard.steps.length -1) {
          this.wizardService.setIsAtLast(this.wizard, true);
        }
      } else {
        this.wizardService.onFinish(this.wizard);
      }
    }
  }

}


/**
 * Simply include the WizardModule in your app module.
 */
@NgModule({
  imports: [CommonModule],
  declarations: [WizardDirective, WizardStepsComponent, WizardStepDirective, WizardPreviousButtonDirective, WizardNextButtonDirective],
  exports: [WizardDirective, WizardStepsComponent, WizardStepDirective, WizardPreviousButtonDirective, WizardNextButtonDirective],
  providers: [WizardService]
})
export class WizardModule {
  static forRoot(): ModuleWithProviders { return {ngModule: WizardModule, providers: []}; }
}

/**
 * Utility - equivalent to jQuery's closest() function
 * @param el
 * @param selector
 * @returns {any}
 */
function getClosest(el, selector) {
  if (typeof selector === 'string') {
    if (el.matches(selector)) return el;
    while (el !== document.body) {
      el = el.parentElement;
      if (el.matches(selector)) return el;
    }
  } else {
    if (el === selector) return el;
    while (el !== document.body) {
      el = el.parentElement;
      if (el === selector) return el;
    }
  }
}
