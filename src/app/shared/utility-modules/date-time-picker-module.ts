/**
 * Wrapper for eonasdan date time picker: http://eonasdan.github.io/bootstrap-datetimepicker/
 * Unfortuanately, the datetimepicker does not update [(ngModel)] on its input.
 * Get around it by using a template reference variable and grabbing the value when needed.
 * General Usage:
 <div class="form-group">
   <div class="input-group date" dateTimePicker [disablePast]="true">
     <input type="text" class="form-control" #myDateInput />
     <span class="input-group-addon">
      <span class="glyphicon glyphicon-calendar"></span>
     </span>
   </div>
 </div>
 *
 * When you want to grab the date, use
 * let myDateString = this.myDateInput.nativeElement.value;
 */

declare const moment;
import {Directive, ElementRef, NgModule, Input} from "@angular/core";

@Directive({
  selector: '[dateTimePicker]'
})
export class BootstrapDateTimePicker{

  @Input('date')
  defaultDate: Date | string;

  @Input('disablePast')
  disablePast: boolean = false;

  @Input('dateFormat')
  dateFormat: string = '';

  @Input('sideBySide')
  sideBySide: boolean = false;

  constructor(
    private el: ElementRef
  ) {}

  ngAfterViewInit() {
    let $el: any = $(this.el.nativeElement);
    let options;
    if (this.disablePast) {
      options = options || {};
      options.minDate = new Date();
    }
    if (this.defaultDate) {
      options = options || {};
      options.defaultDate = this.defaultDate;
    }
    if (this.dateFormat) {
      options = options || {};
      options.format = this.dateFormat;
    }
    if (this.sideBySide) {
      options = options || {};
      options.sideBySide = this.sideBySide;
    }
    $el.datetimepicker(options);
  }
}

@NgModule({
  declarations: [BootstrapDateTimePicker],
  exports: [BootstrapDateTimePicker]
})
export class BootstrapDateTimePickerModule { }
