/**
 * Use this component for a TypeAhead. It supports local and remote data.
 * Simple usage for local data that are strings:
 *
   <input type="text" class="" placeholder="State"
     ctsappTypeahead
     [data]="states"
     [showOnClick]="true"
     (onSelection)="onSimpleStateSelection($event)"
   >
 * Example usage for remote:
 * <input type="text" placeholder="Rider"
     ctsappTypeahead
     [dataSubject]="riderSubject"
     [textProperty]="getRiderName"
     [minLength]="'2'"
     [direction]="'up'"
     (remoteDataCall)="getRiderFromQuery($event)"
     (onSelection)="onRiderSelection($event)"
     (onMatch)="onRiderMatch($event)"
     (onNoMatch)="onRiderNoMatch($event)"
   >
 */

import {ElementRef, Input, AfterViewInit, Component, ViewChild, Renderer, OnDestroy, Output, EventEmitter} from '@angular/core';
import {Subject} from "rxjs/Subject";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";

@Component({
  selector: '[ctsappTypeahead]',
  template: `
    <div ctsappTypeaheadWrapper #wrapper class="wrapper">
      <div *ngIf="filteredData">
        <div class="item" *ngFor="let obj of filteredData; let i = index;" (click)="itemClicked(obj)" [class.suggested]="currentConsiderationIndex == i" (mouseover)="onItemHover(obj, i)">
          {{getDisplayContent(obj)}}
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      .wrapper {
        position: absolute;
        background: white;
        overflow: auto;
        -webkit-box-shadow: 1px 3px 7px rgba(0, 0, 0, 0.5);
        -moz-box-shadow: 1px 3px 7px rgba(0, 0, 0, 0.5);
        box-shadow: 1px 3px 7px rgba(0, 0, 0, 0.5);
      }
      .item {
        padding: 0.3rem;
        font-size: 0.8rem;
        cursor: pointer;
      }
      .suggested {
        background: #427FED;
        color: white;
      }
    `
  ]
})
export class TypeaheadComponent implements AfterViewInit, OnDestroy {

  @Input('data')
  data: any[];

  @Input('dataSubject')
  dataSubject: Subject<any>;

  @Input('minLength')
  minLength = 0;

  @Input('textProperty')
  textProperty = '';

  @Input('showOnClick')
  showOnClick = false;

  @Input('dropdownButton')
  dropdownButton: HTMLElement;

  @Input('direction')
  direction = 'down';

  @Output('onSelection')
  onSelection = new EventEmitter();

  @Output('remoteDataCall')
  remoteDataCall = new EventEmitter();

  @Output('onMatch')
  onMatch = new EventEmitter();

  @Output('onNoMatch')
  onNoMatch = new EventEmitter();

  @ViewChild('wrapper')
  wrapper: ElementRef;

  private searchUpdated: Subject<string> = new Subject<string>();

  private clickListener: Function;
  private blurListener: Function;
  private dropdownClickListener: Function;
  private filteredData: any[] = [];
  private currentConsiderationIndex: number = 0;
  private isShowing = false;

  constructor(
    private el: ElementRef,
    private renderer: Renderer
  ) {
    this.clickListener = renderer.listen(el.nativeElement, 'click', (event) => {
      let query = this.el.nativeElement.value;
      if (query) {
        if (!this.dataSubject) {
          this.filterOnQuery(query);
        }
        this.show();
      } else {
        if (this.showOnClick) {
          this.show();
        }
      }
    });
    this.blurListener = renderer.listenGlobal('body', 'click', (event) => {
      if (this.isShowing) {
        let target = event.target;
        if (!this.getClosest(target, '[ctsappTypeaheadWrapper]') && !event.target.matches('[ctsappTypeahead]')) {
          if (!this.dropdownButton) {
            this.hide();
          } else {
            if (!this.getClosest(target, this.dropdownButton)) {
              this.hide();
            }
          }
        }
        if (!this.filteredData.length) {
          let query = this.el.nativeElement.value;
          this.onNoMatch.emit(query);
        }
        let match = this.checkQueryMatch();
        if (this.checkQueryMatch()) {
          this.doSelect(match);
        }
      }
    });
    el.nativeElement.oninput = this.onInputChange.bind(this);
    el.nativeElement.onkeydown = this.onKeyDown.bind(this);
  }

  ngAfterViewInit() {
    document.body.appendChild(this.el.nativeElement.children[0]);
    this.filteredData = this.data;
    this.wrapper.nativeElement.onmouseout = this.onMouseOut.bind(this);
    if (this.dataSubject) {
      this.dataSubject.subscribe((riders) => {
        this.filteredData = riders;
        this.checkQueryMatch();
      })
      this.searchUpdated.asObservable()
        .debounceTime(200)
        .distinctUntilChanged().subscribe((query) => {
          if (query && query.length >= this.minLength) {
            this.remoteDataCall.emit(query);
          }
      });
    }
    if (this.dropdownButton) {
      this.dropdownClickListener = this.renderer.listen(this.dropdownButton, 'click', (event) => {
        if (this.isShowing) {
          this.hide();
        } else {
          // If the user is clicking the dropdown button, it's likely they want a full selection
          this.filteredData = this.data;
          this.show();
        }
      });
    }
    this.hide();
  }

  checkQueryMatch() {
    let obj = this.filteredData[this.currentConsiderationIndex];
    if (obj) {
      let query = this.el.nativeElement.value;
      let content = this.getDisplayContent(obj);
      if (content && query && content.toLowerCase() === query.toLowerCase()) {
        this.onMatch.emit(obj);
        return obj;
      }
    }
  }

  ngOnDestroy() {
    // Remove listeners
    this.clickListener();
    this.blurListener();
    if (this.dropdownClickListener) {
      this.dropdownClickListener();
    }
  }

  getDisplayContent(obj: any) {
    if (typeof obj !== 'object') {
      return obj;
    }
    return this.getDescendantProperty(obj, this.textProperty)
  }

  show() {
    this.isShowing = true;
    const topPadding = 2;
    const bottomPadding = 10;
    let inputDimensions = this.el.nativeElement.getBoundingClientRect();
    let width = inputDimensions.width;
    let height = inputDimensions.height;
    let top = inputDimensions.top;
    let left = inputDimensions.left;
    let windowHeight = window.innerHeight;
    let wrapperStyle = this.wrapper.nativeElement.style;
    wrapperStyle.display = 'block';
    wrapperStyle.left = `${left}px`;
    wrapperStyle.width = `${width}px`;
    if (this.direction !== 'up') {
      let adjustedTop = top + height + topPadding;
      wrapperStyle.top = `${adjustedTop}px`;
      wrapperStyle.maxHeight = `${windowHeight - adjustedTop - bottomPadding}px`;
    } else {
      let adjustedBottom = windowHeight - (top - topPadding);
      wrapperStyle.bottom = `${adjustedBottom}px`;
      wrapperStyle.maxHeight = `${top - topPadding - bottomPadding}px`;
    }
  }

  hide() {
    this.isShowing = false;
    let wrapperStyle = this.wrapper.nativeElement.style;
    wrapperStyle.display = 'none';
  }

  onItemHover(obj: any, index: number) {
    this.currentConsiderationIndex = index;
  }

  private filterOnQuery(query: string) {
    if (query && query.length >= this.minLength) {
      let regExp = new RegExp('^' + query, 'i');
      this.filteredData = this.data.filter((obj: any) => {
        let value = obj;
        if (typeof obj === 'object') {
          value = this.getDescendantProperty(obj, this.textProperty) || '';
        }
        return regExp.test(value);
      });
      this.checkQueryMatch();
    } else {
      this.filteredData = this.data;
    }
  }

  private onInputChange(event) {
    if (!this.isShowing) {
      this.show();
    }
    let query = event.srcElement.value;
    this.searchUpdated.next(query);
    if (!this.dataSubject) {
      this.filterOnQuery(query);
    }
  }

  private onKeyDown(event) {
    let keyCode = event.which;

    if (keyCode === 13 || keyCode === 9) {
      // Enter || Tab
      event.preventDefault();
      if (this.currentConsiderationIndex) {
        this.doSelect(this.filteredData[this.currentConsiderationIndex]);
      } else {
        let item = this.filteredData[0];
        if (item) {
          this.doSelect(item);
        } else {
          this.hide();
        }
      }
    } else if (keyCode === 38) {
      // Up
      if (this.currentConsiderationIndex > 0) {
        this.currentConsiderationIndex-=1;
      }
    } else if (keyCode === 40) {
      // Down
      if (this.currentConsiderationIndex < this.filteredData.length-1) {
        this.currentConsiderationIndex+=1;
      }
    }
  }

  private onMouseOut() {
    this.currentConsiderationIndex = 0;
  }

  private getDescendantProperty(obj: any, property: string | Function) {
    if (typeof property === 'string') {
      let arr = property.split(".");
      while(arr.length && (obj = obj[arr.shift()]));
      return obj;
    } else {
      let prop = property(obj);
      return prop;
    }
  }

  private doSelect(item: any) {
    this.onSelection.emit(item);
    let value = this.getDisplayContent(item);
    this.el.nativeElement.value = value;
    this.hide();
  }

  private getClosest(el, selector) {
    if (typeof selector === 'string') {
      if (el.matches(selector)) return el;
      while (el !== document.body) {
        el = el.parentElement;
        if (el.matches(selector)) return el;
      }
    } else {
      if (el === selector) return el;
      while (el !== document.body) {
        el = el.parentElement;
        if (el === selector) return el;
      }
    }
  }

  itemClicked(item: any) {
    this.doSelect(item);
  }

}
