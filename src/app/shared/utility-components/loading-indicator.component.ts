/**
 * Use this component for a simple loading overlay while data is being retrieved.
 * Only requirement is that its parent element have 'position: relative'.
 * Example Usage:
   <ctsapp-loading
     [loading]="someLoadingVariable"
     [background]="'inverse'"
     [text]="'Loading...'"
   ></ctsapp-loading>
 */

import {Component, ElementRef, AfterViewInit, Input, ViewChild} from "@angular/core";

enum LoadStates {
  None,
  DoneLoading,
  Loading
}

@Component({
  selector: 'ctsapp-loading',
  template: `
    <div #wrapper class="wrapper" [hidden]="state === LoadStates.None" [class.fade]="state === LoadStates.DoneLoading" [ngClass]="backgroundClass">
      <div class="spinner" [hidden]="!showSpinner" [ngClass]="spinnerClass"></div>
      <div #innerText [class.fade]="!showText" class="inner-text">{{text}} {{progress}} {{progress ? '%' : ''}}</div>
    </div>
  `,
  styles: [
    `
      .wrapper {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: rgba(255,255,255,0.7);
        color: rgba(0,0,0,0.8);
      }
      .wrapper.inverted {
        background: rgba(0,0,0,0.6);
        color: rgba(255,255,255,0.7);
      }
      .wrapper.transparent {
        background: transparent;
      }
      .wrapper.fade {
        opacity: 0;
      }
      .inner-text {
        font-size: 0.8rem;
        top: calc(50% + 1.9rem);
        left: calc(50% - 2.3rem);
        position: absolute;
        font-weight: 300;
        opacity: 1;
        transition: opacity 100ms;
      }
      .inner-text.fade {
        opacity: 0;
      }
      .spinner {
        font-size: 1em;
        margin: 0px;
        text-align: center;
        z-index: 1000;
        height: 100%;
      }
      .spinner:before {
        position: absolute;
        content: '';
        left: calc(50% - 1.15rem);
        top: calc(50% - 1.15rem);
        width: 2.29rem;
        height: 2.29rem;
        border-radius: 500rem;
        border: .2em solid rgba(255,255,255,.15);
      }
      .spinner:after {
        border-color: #FFF transparent transparent;
        position: absolute;
        content: '';
        left: calc(50% - 1.15rem);
        top: calc(50% - 1.15rem);
        width: 2.29rem;
        height: 2.29rem;
        -webkit-animation: loader .6s linear;
        animation: loader .6s linear;
        -webkit-animation-iteration-count: infinite;
        animation-iteration-count: infinite;
        border-radius: 500rem;
        border-color: #767676 transparent transparent;
        border-style: solid;
        border-width: .2em;
        box-shadow: 0 0 0 1px transparent;
      }
      .spinner.inverted:before {
        border-color: rgba(255,255,255,.1);
      }
      .spinner.inverted:after {
        border-color: #C7C7C7 transparent transparent;
      }
      @keyframes loader {
        from {
          transform: rotate(0deg);
        }
        to {
          transform: rotate(360deg);
        }
      }
    `
  ]
})
export class LoadingIndicatorComponent implements AfterViewInit {

  @ViewChild('wrapper')
  wrapper: ElementRef;

  @ViewChild('innerText')
  innerText: ElementRef;

  private LoadStates = LoadStates;
  private state = LoadStates.None;
  private _loading = false;
  private fadeTimeout = null;
  private delayTimeout = null;
  private showText = false;
  private backgroundClass = {
    inverted: false,
    transparent: false
  }
  private spinnerClass = {
    inverted: false
  }

  @Input('loading')
  set loading(isLoading: boolean) {
    let wasLoading = this._loading;
    this._loading = isLoading;
    if (!isLoading && wasLoading) {
      // Done Loading
      this.state = LoadStates.DoneLoading;
      clearTimeout(this.delayTimeout);
      clearTimeout(this.fadeTimeout);
      this.fadeTimeout = setTimeout(() => {
        this.state = LoadStates.None;
      }, this.fadeTime);
    } else if (isLoading && !wasLoading) {
      // Start Loading
      clearTimeout(this.delayTimeout);
      clearTimeout(this.fadeTimeout);
      this.delayTimeout = setTimeout(() => {
        this.state = LoadStates.Loading;
        setTimeout(() => {
          if (this.text) {
            let textDimensions = this.innerText.nativeElement.getBoundingClientRect();
            this.innerText.nativeElement.style.left = `calc(50% - ${textDimensions.width/2}px)`;
            if (!this.showSpinner) {
              this.innerText.nativeElement.style.top = `calc(50% - ${textDimensions.height/2}px)`;
            }
            this.showText = true;
          }
        }, 1)
      }, this.delay);
    }
  }

  @Input('background')
  background: string;

  @Input('delay')
  delay = 0;

  @Input('text')
  text = '';

  @Input('showSpinner')
  showSpinner = true;

  @Input('fadeTime')
  fadeTime = 300;

  @Input('progress')
  progress = '';

  constructor(
    private el: ElementRef
  ) { }

  ngAfterViewInit() {
    let nativeEl: HTMLElement = this.el.nativeElement;
    let parent = <HTMLElement>nativeEl.parentNode;
    if (parent.style.position !== 'relative') {
      console.warn('Adding position: relative to the parent element of the loading indicator.');
      parent.style.position = 'relative';
    }
    nativeEl.style.transition = `opacity ${this.fadeTime}ms`;
    this.backgroundClass = {
        inverted: this.background === 'inverse',
        transparent: this.background === 'none'
      }
    this.spinnerClass = {
        inverted: this.background === 'inverse'
      }
  }

}
