import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {HttpModule} from "@angular/http";
import {AppComponent} from "./app.component";
import {AppRoutingModule} from "./app-routing.module";
import {AuthService} from "./shared/auth/auth.service";
import {DashboardModule} from "./dashboard/dashboard.module";
import {ErrorService} from "./shared/utility-services/error.service";
import {ToastModule} from "ng2-toastr/ng2-toastr";

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    DashboardModule,
    ToastModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    AuthService,
    ErrorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
