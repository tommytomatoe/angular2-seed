/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DashboardFormsComponent } from './dashboard-forms.component';
import {MaskService} from "../../../shared/utility-services/mask.service";
import {TextMaskModule} from "angular2-text-mask";
import {FormsModule} from "@angular/forms";
import {RiderService} from "../../../shared/api/rider.service";
import {ErrorService} from "../../../shared/utility-services/error.service";
import {PassService} from "../../../shared/api/pass.service";

class PassServiceStub {}
class RiderServiceStub {}
class ErrorServiceStub {}

describe('DashboardFormsComponent', () => {
  let component: DashboardFormsComponent;
  let fixture: ComponentFixture<DashboardFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TextMaskModule, FormsModule],
      declarations: [ DashboardFormsComponent ],
      providers: [
        MaskService,
        {provide: RiderService, userClass: RiderServiceStub},
        {provide: PassService, userClass: PassServiceStub},
        {provide: ErrorService, userClass: ErrorServiceStub},
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
