import {Component, OnInit, ViewChild} from "@angular/core";
import {Pass} from "../../../shared/api/pass.model";
import {MaskService} from "../../../shared/utility-services/mask.service";
import {PassService} from "../../../shared/api/pass.service";
import {ErrorService} from "../../../shared/utility-services/error.service";
import {RiderService} from "../../../shared/api/rider.service";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-dashboard-forms',
  templateUrl: './dashboard-forms.component.html',
  styleUrls: ['./dashboard-forms.component.scss']
})
export class DashboardFormsComponent implements OnInit {

  @ViewChild('passForm')
  passForm: NgForm;

  passFormErrors = {
    riderId: ''
  }
  passFormValidationMessages = {
    riderId: {
      'required':      'Rider ID is required.',
      'invalidId':     'Rider ID does not exist! Please try again.'
    }
  }

  newPassDetails = {
    riderId: '',
    expirationDate: ''
  }
  createdPass: Pass = null;

  riderIdMask: any;
  dateMask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
  datePipe = null;
  twentyFourHourTimeMask = [/\d/, /\d/, ':', /[0-5]/, /\d/];
  twentyFourHourTimePipe = null;
  socialSecurityMask = [/\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  emailMask = null;
  emailPipe = null;

  didSubmit = false;
  successfullySubmitted = false;

  constructor(
    private passService: PassService,
    private riderService: RiderService,
    private errorService: ErrorService,
    private textMaskService: MaskService
  ) {
    this.riderIdMask = this.textMaskService.createNumberMask({prefix: '', includeThousandsSeparator: false});
    let date = new Date();
    this.datePipe = this.textMaskService.createAutoCorrectedDatePipe();
    //this.datePipe = this.textMaskService.createAutoCorrectedDatePipe(null, {'dd': date.getDate(), 'mm': date.getMonth()+1, 'yyyy': date.getFullYear()}, {'dd': 31, 'mm': 12, 'yyyy': 2018});
    this.twentyFourHourTimePipe = this.textMaskService.createTwentyFourHourTimePipe();
    this.emailMask = this.textMaskService.createEmailMask();
    this.emailPipe = this.textMaskService.createEmailPipe();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    if (this.passForm) {
      this.passForm.valueChanges
        .subscribe(data => this.onPassFormValueChanged(data));
    }
  }

  // onBadDate(details) {
  //   let conformedValue = details.conformedValue;
  //   let maskRejection = details.maskRejection;
  //   let pipeRejection = details.pipeRejection;
  //
  //   console.log('bad date');
  //   console.log(details);
  // }

  onPassFormValueChanged(data?: any) {
    if (!this.passForm) {
      return;
    }
    const form = this.passForm.form;
    for (const field in this.passFormErrors) {
      // clear previous error message (if any)
      this.passFormErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.passFormValidationMessages[field];
        for (const key in control.errors) {
          this.passFormErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onRiderIdBlur() {
    this.checkRiderIdExists();
  }

  onSubmit() {
    this.didSubmit = true;
    let createdDate = new Date().toString();
    let expirationDate;
    if (!this.newPassDetails.expirationDate) {
      // Two weeks from now
      this.newPassDetails.expirationDate = new Date(+new Date + 12096e5).toString();
    }
    let pass = new Pass({
      riderId: +this.newPassDetails.riderId,
      expirationDate: this.newPassDetails.expirationDate,
      createdDate: createdDate
    });
    this.passService.create(pass.model).subscribe((pass) => {
      this.createdPass = pass;
      this.successfullySubmitted = true;
    }, err => {
      if (err.status === 0) {
        // The mock server was unreachable, display a message
        this.errorService.serverUnreachable();
      } else {
        this.errorService.unknown(err);
      }
    });
  }

  private checkRiderIdExists() {
    if (this.newPassDetails.riderId) {
      this.riderService.getById(+this.newPassDetails.riderId).subscribe((rider) => {
        this.passFormErrors.riderId = '';
      }, err => {
        this.didSubmit = false;
        if (err.status === 404) {
          this.passFormErrors.riderId = this.passFormValidationMessages.riderId.invalidId;
        }
      });
    }
  }

}
