/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, ViewContainerRef } from '@angular/core';


import { DashboardToastComponent } from './dashboard-toast.component';
import { FormsModule } from "@angular/forms";
import { ToastsManager } from "ng2-toastr/ng2-toastr";

class ToastManagerStub {
  setRootViewContainerRef() {
  }
  success() {
  }
}

class ViewContainerRefStub {}

describe('DashboardToastComponent', () => {
  let component: DashboardToastComponent;
  let fixture: ComponentFixture<DashboardToastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [DashboardToastComponent],
      providers: [ViewContainerRef,
        {provide: ToastsManager, useClass: ToastManagerStub}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardToastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
