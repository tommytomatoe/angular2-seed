import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-dashboard-toast',
  templateUrl: './dashboard-toast.component.html',
  styleUrls: ['./dashboard-toast.component.scss']
})
export class DashboardToastComponent implements OnInit {

  toastTitle = 'You are awesome!';
  toastMessage = 'Success!';
  toastMode = '';

  constructor(public toastr: ToastsManager, vRef: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vRef);
  }

  ngOnInit() {

  }

  // ------------------------------------------------------------------- UI Events

  showToastClicked() {
    this.toastr.success(this.toastTitle, this.toastMessage);
  }

}
