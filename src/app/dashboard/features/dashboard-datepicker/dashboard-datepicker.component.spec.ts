/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DashboardDatepickerComponent } from './dashboard-datepicker.component';
import {FormsModule} from "@angular/forms";
import {BootstrapDateTimePickerModule} from "../../../shared/utility-modules/date-time-picker-module";

describe('DashboardDatepickerComponent', () => {
  let component: DashboardDatepickerComponent;
  let fixture: ComponentFixture<DashboardDatepickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, BootstrapDateTimePickerModule],
      declarations: [ DashboardDatepickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardDatepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
