/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import {DebugElement, enableProdMode} from '@angular/core';

import { DashboardTypeaheadComponent } from './dashboard-typeahead.component';
import {TypeaheadComponent} from "../../../shared/utility-components/typeahead.component";
import {RiderService} from "../../../shared/api/rider.service";
import {Observable} from "rxjs";

class RiderServiceStub {
  search(){
    return new Observable<any>((subscriber) => {
      subscriber.next([{}]);
      subscriber.complete();
    })
  }
}

describe('DashboardTypeaheadComponent', () => {
  let component: DashboardTypeaheadComponent;
  let fixture: ComponentFixture<DashboardTypeaheadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardTypeaheadComponent, TypeaheadComponent ],
      providers: [
        { provide: RiderService, useClass: RiderServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardTypeaheadComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
