import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-wizard',
  templateUrl: './dashboard-wizard.component.html',
  styleUrls: ['./dashboard-wizard.component.scss']
})
export class DashboardWizardComponent implements OnInit {

  step1Info = 'bind information in any of these like a regular component.';
  didFinish = false;
  inputValue = '';
  nextEnabled = true;

  constructor() { }

  ngOnInit() {
  }

  onFinish() {
    this.didFinish = true;
  }

  onInputChange() {
    this.nextEnabled = !!this.inputValue.length;
  }

  onNext(index: number) {
    if (index == 1) {
      this.nextEnabled = !!this.inputValue.length;
    } else {
      this.nextEnabled = true;
    }
  }

  onPrevious(index: number) {
    if (index == 1) {
      this.nextEnabled = !!this.inputValue.length;
    } else {
      this.nextEnabled = true;
    }
  }

}
