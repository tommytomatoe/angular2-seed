/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DashboardWizardComponent } from './dashboard-wizard.component';
import {WizardModule} from "../../../shared/utility-directives/wizard.directive";
import {FormsModule} from "@angular/forms";

describe('DashboardWizardComponent', () => {
  let component: DashboardWizardComponent;
  let fixture: ComponentFixture<DashboardWizardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [WizardModule, FormsModule],
      declarations: [ DashboardWizardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardWizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
