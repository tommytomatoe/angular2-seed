import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-loading-indicator',
  templateUrl: './dashboard-loading-indicator.component.html',
  styleUrls: ['./dashboard-loading-indicator.component.scss']
})
export class DashboardLoadingIndicatorComponent implements OnInit {

  firstBlockLoading = false;
  secondBlockLoading = false;
  thirdBlockLoading = false;
  fourthBlockLoading = false;

  progressInterval = null;
  progress = 0;

  constructor(

  ) { }

  ngOnInit() {

  }

  // ------------------------------------------------------------------- UI Events

  simulateFirstBlockLoad() {
    this.firstBlockLoading = true;
    setTimeout(() => {
      this.firstBlockLoading = false;
    }, 2000);
  }

  simulateSecondBlockLoad() {
    this.secondBlockLoading = true;
    setTimeout(() => {
      this.secondBlockLoading = false;
    }, 2000);
  }

  simulateThirdBlockLoad() {
    this.progress = 0;
    this.thirdBlockLoading = true;
    clearInterval(this.progressInterval);
    this.progressInterval = setInterval(() => {
      this.progress+=1;
      if (this.progress >= 99) {
        this.thirdBlockLoading = false;
        clearInterval(this.progressInterval);
      }
    }, 20);
  }

  simulateFourthBlockLoad() {
    this.fourthBlockLoading = true;
    setTimeout(() => {
      this.fourthBlockLoading = false;
    }, 2000);
  }

}
