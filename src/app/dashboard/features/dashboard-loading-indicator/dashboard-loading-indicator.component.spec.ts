/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DashboardLoadingIndicatorComponent } from './dashboard-loading-indicator.component';
import {LoadingIndicatorComponent} from "../../../shared/utility-components/loading-indicator.component";

describe('DashboardLoadingIndicatorComponent', () => {
  let component: DashboardLoadingIndicatorComponent;
  let fixture: ComponentFixture<DashboardLoadingIndicatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardLoadingIndicatorComponent, LoadingIndicatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardLoadingIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
