import { Component, OnInit } from '@angular/core';
import {Transaction} from "../../../shared/api/transaction.model";
import {TransactionService} from "../../../shared/api/transaction.service";
import {Pass} from "../../../shared/api/pass.model";
import {PassService} from "../../../shared/api/pass.service";

@Component({
  selector: 'app-dashboard-drag-drop',
  templateUrl: './dashboard-drag-drop.component.html',
  styleUrls: ['./dashboard-drag-drop.component.scss']
})
export class DashboardDragDropComponent implements OnInit {

  pass: Pass;
  transactionProperties = [
    'passId',
    'rideId',
    'amount',
  ]
  transactions: Transaction[] = [];
  recentlyDroppedTransaction: Transaction;

  zone1Items = [
    'Drag from one zone to another',
    'Try dragging this one'
  ]
  zone2Items = [
    'Try dragging from Zone 1 to here',
    'And try dragging from here to Zone 1'
  ]

  constructor(
    private transactionService: TransactionService,
    private passService: PassService
  ) { }

  ngOnInit() {
    this.transactionService.getAll().subscribe((transactions) => {
      this.transactions = transactions;
    });
    this.passService.getById(100).subscribe((pass) => {
      this.pass = pass;
    });
  }

  onZone1Drop(item: string) {
    // Remove from zone 2 and add to zone 1
    let index = this.zone2Items.indexOf(item);
    if (index > -1) {
      this.zone2Items.splice(index, 1);
    }
    this.zone1Items.push(item);
  }

  onZone2Drop(item: string) {
    // Remove from zone 1 and add to zone 2
    let index = this.zone1Items.indexOf(item);
    if (index > -1) {
      this.zone1Items.splice(index, 1);
    }
    this.zone2Items.push(item);
  }

  onTransactionDrop(item: Transaction) {
    this.recentlyDroppedTransaction = item;
    item.model.passId = this.pass.model.id;
    this.transactionService.update(item.model).subscribe((transaction) => {
      console.log(transaction);
    });
  }

}
