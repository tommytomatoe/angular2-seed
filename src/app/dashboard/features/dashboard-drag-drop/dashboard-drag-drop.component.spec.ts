/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DashboardDragDropComponent } from './dashboard-drag-drop.component';
import {TransactionService} from "../../../shared/api/transaction.service";
import {CtsAppDragDropModule} from "../../../shared/utility-directives/drag-drop.module";
import {PassService} from "../../../shared/api/pass.service";
import {Observable} from "rxjs";

class TransactionServiceStub {
  getAll() {
    return new Observable<any>((subscriber) => {
      subscriber.next([]);
      subscriber.complete();
    })
  }
}
class PassServiceStub {
  getById() {
    return new Observable<any>((subscriber) => {
      subscriber.next({model: {}});
      subscriber.complete();
    })
  }
}

describe('DashboardDragDropComponent', () => {
  let component: DashboardDragDropComponent;
  let fixture: ComponentFixture<DashboardDragDropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CtsAppDragDropModule],
      declarations: [ DashboardDragDropComponent ],
      providers: [
        {provide: TransactionService, useClass: TransactionServiceStub},
        {provide: PassService, useClass: PassServiceStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardDragDropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
