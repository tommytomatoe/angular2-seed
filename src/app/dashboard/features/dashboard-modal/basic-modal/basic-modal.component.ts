import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from "../../../../shared/utility-services/modal.service";

@Component({
  selector: '[appBasicModal]',
  templateUrl: './basic-modal.component.html',
  styleUrls: ['./basic-modal.component.scss']
})
export class BasicModalComponent implements OnInit {

  constructor(
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
  }

}
