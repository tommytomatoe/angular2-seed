import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-routed-modal',
  templateUrl: './routed-modal.component.html',
  styleUrls: ['./routed-modal.component.scss']
})
export class RoutedModalComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  closeExButtonClicked() {
    this.router.navigate(['/dashboard/modal']);
  }

  closeButtonClicked() {
    this.router.navigate(['/dashboard/modal']);
  }

}
