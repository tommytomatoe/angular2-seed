import { Component, OnInit } from '@angular/core';
// import {ModalService} from "../../../shared/utility-services/modal.service";
import {BasicModalComponent} from "./basic-modal/basic-modal.component";
import {NgbModal, NgbModalRef, ConfirmComponentResult} from "../../../shared/utility-services/modal.service";
import {Router, NavigationEnd} from "@angular/router";
import {RoutedModalComponent} from "./routed-modal/routed-modal.component";
import {InputsModalComponent} from "./inputs-modal/inputs-modal.component";
import {Rider} from "../../../shared/api/rider.model";

@Component({
  selector: 'app-dashboard-modal',
  templateUrl: './dashboard-modal.component.html',
  styleUrls: ['./dashboard-modal.component.scss']
})
export class DashboardModalComponent implements OnInit {

  routedModal: NgbModalRef;
  confirmResult: string = null;

  constructor(
    private modalService: NgbModal,
    private router: Router
  ) {
    router.events.subscribe((val) => {
      // Just one way of doing it
      if (val instanceof NavigationEnd) {
        if (val.url.indexOf('routed-modal') > -1) {
          // Navigating to
          if (!this.routedModal) {
            this.routedModal = this.modalService.open(RoutedModalComponent, {backdrop: 'static', keyboard: false, size: 'lg'});
            this.routedModal.componentInstance.name = 'World';
          }
        } else {
          // Navigating from
          if (this.routedModal) {
            this.routedModal.close();
          }
        }
      }
    });
  }

  ngOnInit() {
  }

  openSimpleModalClicked() {
    // The other option is to include it inline and do stuff by ID
    // this.modalService.open(BasicModalComponent);
    this.modalService.open(BasicModalComponent);
  }

  modalWithInputsClicked() {
    const modalRef = this.modalService.open(InputsModalComponent, {size: 'lg'});
    const exampleRider: Rider = new Rider({
      riderId: 150,
      pin: 151,
      name: {
        first: 'John',
        last: 'Rider'
      },
      email: 'john.rider@undefined.com',
      phone: '555-555-5555',
      address: '123 Abc Street'
    });
    modalRef.componentInstance.rider = exampleRider;
  }

  openConfirmationModalClicked() {
    this.modalService.confirm({
      title: 'Confirm',
      message: 'Are you sure you want to take some action?'
    }).result.then((val: ConfirmComponentResult) => {
      switch (val) {
        case ConfirmComponentResult.Yes:
          this.confirmResult = 'Yes';
          break;
        case ConfirmComponentResult.No:
          this.confirmResult = 'No';
          break;
        case ConfirmComponentResult.Dismiss:
          this.confirmResult = 'Dismissed';
          break;
      }
    });
  }

}
