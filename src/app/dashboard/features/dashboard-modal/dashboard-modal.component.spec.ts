/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DashboardModalComponent } from './dashboard-modal.component';
import {RouterStub} from "../../../../testing/router-stub";
import {Router} from "@angular/router";
import {NgbModal} from "../../../shared/utility-services/modal.service";

class NgbModalStub {

}

describe('DashboardModalComponent', () => {
  let component: DashboardModalComponent;
  let fixture: ComponentFixture<DashboardModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardModalComponent ],
      providers: [{ provide: Router, useClass: RouterStub }, {provide: NgbModal, useClass: NgbModalStub}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
