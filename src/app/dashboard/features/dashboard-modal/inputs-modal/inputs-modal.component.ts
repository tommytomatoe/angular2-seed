import { Component, OnInit } from '@angular/core';
import {Rider} from "../../../../shared/api/rider.model";
import {NgbActiveModal} from "../../../../shared/utility-services/modal.service";

@Component({
  selector: 'app-inputs-modal',
  templateUrl: './inputs-modal.component.html',
  styleUrls: ['./inputs-modal.component.scss']
})
export class InputsModalComponent implements OnInit {

  riderProperties = Rider.getProperties();
  rider: Rider;

  constructor(
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
  }

}
