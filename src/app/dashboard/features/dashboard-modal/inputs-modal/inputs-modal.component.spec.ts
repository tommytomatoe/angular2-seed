/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { InputsModalComponent } from './inputs-modal.component';
import {NgbActiveModal} from "../../../../shared/utility-services/modal.service";

class NgbActiveModalStub {

}

describe('InputsModalComponent', () => {
  let component: InputsModalComponent;
  let fixture: ComponentFixture<InputsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputsModalComponent ],
      providers: [{provide: NgbActiveModal, useClass: NgbActiveModalStub}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
