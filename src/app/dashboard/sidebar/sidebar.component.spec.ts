/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import {SidebarComponent, SidebarService} from './sidebar.component';
import {BootstrapTourModule} from "../../shared/utility-directives/bootstrap-tour.module";
import {RouterLinkActiveStubDirective} from "../../../testing/router-stub";
import {AuthService} from "../../shared/auth/auth.service";

let mockAuthServiceStub = {
  getLoggedInUser: function() {
    return {
      isAdmin: false
    }
  }
}

describe('SidebarComponent', () => {
  let component: SidebarComponent;
  let fixture: ComponentFixture<SidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BootstrapTourModule],
      declarations: [ SidebarComponent, RouterLinkActiveStubDirective ],
      providers: [SidebarService, {provide: AuthService, useValue: mockAuthServiceStub }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
