import {Component, OnInit, Injectable} from '@angular/core';
import {AuthService} from "../../shared/auth/auth.service";
import {LoggedInUser} from "../../shared/auth/iauth.service";

@Injectable()
export class SidebarService {
  isOpen = true;
}

@Component({
  selector: '[app-sidebar]',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  loggedInUser: LoggedInUser;

  constructor(
    private sidebarService: SidebarService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.loggedInUser = this.authService.getLoggedInUser();
  }

  // ------------------------------------------------------------------- UI Events

  closeSidebarClicked() {
    this.sidebarService.isOpen = false;
  }

}
