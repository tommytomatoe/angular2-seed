import {Component, OnInit, Injectable} from '@angular/core';

@Injectable()
export class NotificationSidebarService {
  isOpen = false;
}

@Component({
  selector: 'app-notification-sidebar',
  templateUrl: './notification-sidebar.component.html',
  styleUrls: ['./notification-sidebar.component.scss']
})
export class NotificationSidebarComponent implements OnInit {

  notifications = NOTIFICATIONS;

  constructor(private notificationSidebarService: NotificationSidebarService) { }

  ngOnInit() {
  }

}

const NOTIFICATIONS = [
    {
      title: 'Notification 1',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      timestamp: new Date((new Date()).valueOf() - 1000*60*60*24)
    },
    {
      title: 'Notification 2',
      content: 'Praesent nec rhoncus tellus. In lacus lectus, condimentum sit amet facilisis vitae, congue vitae magna. Sed lacinia lobortis urna.',
      timestamp: new Date((new Date()).valueOf() - 1000*60*60*48)
    },
    {
      title: 'Notification 3',
      content: 'Donec sagittis nibh vel volutpat vulputate. Integer laoreet arcu massa, ac tempor velit convallis at.',
      timestamp: new Date((new Date()).valueOf() - 1000*60*60*72)
    },
    {
      title: 'Notification 4',
      content: 'Mauris varius augue quam, non ullamcorper purus condimentum non. Aliquam odio enim, elementum id aliquet in, facilisis nec ante..',
      timestamp: new Date((new Date()).valueOf() - 1000*60*60*96)
    },
    {
      title: 'Notification 5',
      content: 'Fusce ac turpis at nibh finibus auctor.',
      timestamp: new Date((new Date()).valueOf() - 1000*60*60*120)
    }
]
