/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import {NotificationSidebarComponent, NotificationSidebarService} from './notification-sidebar.component';

describe('NotificationSidebarComponent', () => {
  let component: NotificationSidebarComponent;
  let fixture: ComponentFixture<NotificationSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationSidebarComponent ],
      providers: [NotificationSidebarService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
