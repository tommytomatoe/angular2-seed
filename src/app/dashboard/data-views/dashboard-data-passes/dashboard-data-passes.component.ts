import { Component, OnInit } from '@angular/core';
import {Pass} from "../../../shared/api/pass.model";
import {PassService} from "../../../shared/api/pass.service";
import {ErrorService} from "../../../shared/utility-services/error.service";

@Component({
  selector: 'app-dashboard-data-passes',
  templateUrl: './dashboard-data-passes.component.html',
  styleUrls: ['./dashboard-data-passes.component.scss']
})
export class DashboardDataPassesComponent implements OnInit {

  passProperties = Pass.getProperties();
  passes: Pass[] = [];

  constructor(
    private passService: PassService,
    private errorService: ErrorService
  ) { }

  ngOnInit() {
    this.passService.getAll().subscribe((passes) => {
      this.passes = passes;
    }, err => {
      if (err.status === 0) {
        // The mock server was unreachable, display a message
        this.errorService.serverUnreachable()
      } else {
        this.errorService.unknown(err);
      }
    });
  }

}
