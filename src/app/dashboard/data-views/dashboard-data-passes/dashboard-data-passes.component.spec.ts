/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DashboardDataPassesComponent } from './dashboard-data-passes.component';
import {Observable} from "rxjs";
import {ErrorService} from "../../../shared/utility-services/error.service";
import {PassService} from "../../../shared/api/pass.service";

class PassServiceStub {
  getAll() {
    return new Observable<any>((subscriber) => {
      subscriber.next([]);
      subscriber.complete();
    })
  }
}
class ErrorServiceStub {

}

describe('DashboardDataPassesComponent', () => {
  let component: DashboardDataPassesComponent;
  let fixture: ComponentFixture<DashboardDataPassesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardDataPassesComponent ],
      providers: [
        {provide: PassService, useClass: PassServiceStub},
        {provide: ErrorService, useClass: ErrorServiceStub},
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardDataPassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
