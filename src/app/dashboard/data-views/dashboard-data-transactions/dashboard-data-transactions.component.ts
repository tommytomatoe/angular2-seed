import { Component, OnInit } from '@angular/core';
import {Transaction} from "../../../shared/api/transaction.model";
import {TransactionService} from "../../../shared/api/transaction.service";
import {ErrorService} from "../../../shared/utility-services/error.service";

@Component({
  selector: 'app-dashboard-data-transactions',
  templateUrl: './dashboard-data-transactions.component.html',
  styleUrls: ['./dashboard-data-transactions.component.scss']
})
export class DashboardDataTransactionsComponent implements OnInit {

  transactionProperties = Transaction.getProperties();
  transactions: Transaction[] = [];

  constructor(
    private transactionService: TransactionService,
    private errorService: ErrorService
  ) { }

  ngOnInit() {
    this.transactionService.getAll().subscribe((transactions) => {
      this.transactions = transactions;
    }, err => {
      if (err.status === 0) {
        // The mock server was unreachable, display a message
        this.errorService.serverUnreachable()
      } else {
        this.errorService.unknown(err);
      }
    });
  }

}
