/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DashboardDataTransactionsComponent } from './dashboard-data-transactions.component';
import {TransactionService} from "../../../shared/api/transaction.service";
import {ErrorService} from "../../../shared/utility-services/error.service";
import {Observable} from "rxjs";

class TransactionServiceStub {
  getAll() {
    return new Observable<any>((subscriber) => {
      subscriber.next([]);
      subscriber.complete();
    })
  }
}
class ErrorServiceStub {

}

describe('DashboardDataTransactionsComponent', () => {
  let component: DashboardDataTransactionsComponent;
  let fixture: ComponentFixture<DashboardDataTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardDataTransactionsComponent ],
      providers: [
        {provide: TransactionService, useClass: TransactionServiceStub},
        {provide: ErrorService, useClass: ErrorServiceStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardDataTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
