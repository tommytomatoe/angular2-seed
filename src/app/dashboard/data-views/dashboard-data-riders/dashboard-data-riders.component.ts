import { Component, OnInit } from '@angular/core';
import {Rider} from "../../../shared/api/rider.model";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-dashboard-data-riders',
  templateUrl: './dashboard-data-riders.component.html',
  styleUrls: ['./dashboard-data-riders.component.scss']
})
export class DashboardDataRidersComponent implements OnInit {

  riderProperties = Rider.getProperties();
  riders: Rider[] = [];

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    // We use the resolve route to grab the data before showing the page
    this.route.data.subscribe((data: { riders: Rider[] }) => {
      this.riders = data.riders;
    });
  }

}
