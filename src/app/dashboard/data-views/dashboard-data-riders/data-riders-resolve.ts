import { Injectable }             from '@angular/core';
import {
  Router, Resolve,
  ActivatedRouteSnapshot, RouterStateSnapshot
} from '@angular/router';
import {Rider} from "../../../shared/api/rider.model";
import {RiderService} from "../../../shared/api/rider.service";
import {Observable} from "rxjs/Rx";

@Injectable()
export class DataRidersResolve implements Resolve<Rider[]> {

  constructor(
    private riderService: RiderService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Rider[]> {
    return new Observable<Rider[]>((observer) => {
      this.riderService.getAll().subscribe((riders) => {
        observer.next(riders);
        observer.complete();
      }, err => {
        // There was an error - reroute to home
        this.router.navigate(['/dashboard']);
        return false;
      });
    });
  }
}
