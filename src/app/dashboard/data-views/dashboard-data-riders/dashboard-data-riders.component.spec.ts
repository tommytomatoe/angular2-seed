/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import {DebugElement, Injectable} from '@angular/core';

import { DashboardDataRidersComponent } from './dashboard-data-riders.component';
import {CommonModule} from "@angular/common";
import {Observable} from "rxjs/Rx";
import {Rider} from "../../../shared/api/rider.model";
import {ActivatedRoute} from "@angular/router";

@Injectable()
export class RiderActivatedRouteStub {
  data = {
    subscribe: (callback: Function) => {
      callback({
        riders: [
          new Rider({
            id: 1,
            riderId: 1,
            pin: 1,
            name: {
              first: 'joe',
              last: 'test'
            },
            email: 'test',
            phone: 'test',
            address: 'test'
          }),
          new Rider({
            id: 2,
            riderId: 2,
            pin: 2,
            name: {
              first: 'joe2',
              last: 'test2'
            },
            email: 'test2',
            phone: 'test2',
            address: 'test2'
          })
        ]
      })
    }
  }
}

describe('DashboardDataRidersComponent', () => {
  let component: DashboardDataRidersComponent;
  let fixture: ComponentFixture<DashboardDataRidersComponent>;

  let errorServiceStub = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule
      ],
      declarations: [ DashboardDataRidersComponent ],
      providers: [
        { provide: ActivatedRoute, useClass: RiderActivatedRouteStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardDataRidersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have two riders in the table', () => {
    let de = fixture.debugElement.query(By.css('tbody'));
    expect(de.children.length).toBe(2);
  });
});
