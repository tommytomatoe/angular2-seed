import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import {DashboardHomeComponent} from "./dashboard-home/dashboard-home.component";
import {DashboardHomeView2Component} from "./dashboard-home/dashboard-home-view2/dashboard-home-view2.component";
import {DashboardHomeView1Component} from "./dashboard-home/dashboard-home-view1/dashboard-home-view1.component";
import {DashboardAdminComponent} from "./dashboard-admin/dashboard-admin.component";
import {DashboardAdminAuthGuard} from "./dashboard-admin-auth-guard.service";
import {DashboardAuthGuard} from "./dashboard-auth-guard.service";
import {RiderService} from "../shared/api/rider.service";
import {DashboardRoutingModule} from "./dashboard-routing.module";
import {TransactionService} from "../shared/api/transaction.service";
import {PassService} from "../shared/api/pass.service";
import {SidebarComponent, SidebarService} from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { DashboardDataRidersComponent } from './data-views/dashboard-data-riders/dashboard-data-riders.component';
import { DashboardDataTransactionsComponent } from './data-views/dashboard-data-transactions/dashboard-data-transactions.component';
import { DashboardDataPassesComponent } from './data-views/dashboard-data-passes/dashboard-data-passes.component';
import { DashboardToastComponent } from './features/dashboard-toast/dashboard-toast.component';
import { DashboardTypeaheadComponent } from './features/dashboard-typeahead/dashboard-typeahead.component';
import {TypeaheadComponent} from "../shared/utility-components/typeahead.component";
import { DashboardFormsComponent } from './features/dashboard-forms/dashboard-forms.component';
import {FormsModule} from "@angular/forms";
import { TextMaskModule } from 'angular2-text-mask';
import {MaskService} from "../shared/utility-services/mask.service";
import {LoadingIndicatorComponent} from "../shared/utility-components/loading-indicator.component";
import {DashboardLoadingIndicatorComponent} from "./features/dashboard-loading-indicator/dashboard-loading-indicator.component";
import { DashboardModalComponent } from './features/dashboard-modal/dashboard-modal.component';
import { BasicModalComponent } from './features/dashboard-modal/basic-modal/basic-modal.component';
import {NgbModalModule, NgbModalStack} from "../shared/utility-services/modal.service";
import { RoutedModalComponent } from './features/dashboard-modal/routed-modal/routed-modal.component';
import { InputsModalComponent } from './features/dashboard-modal/inputs-modal/inputs-modal.component';
import { DashboardDatepickerComponent } from './features/dashboard-datepicker/dashboard-datepicker.component';
import { DashboardWizardComponent } from './features/dashboard-wizard/dashboard-wizard.component';
import {WizardModule} from "../shared/utility-directives/wizard.directive";
import {BootstrapTourModule} from "../shared/utility-directives/bootstrap-tour.module";
import { DashboardDragDropComponent } from './features/dashboard-drag-drop/dashboard-drag-drop.component';
import {CtsAppDragDropModule} from "../shared/utility-directives/drag-drop.module";
import {DataRidersResolve} from "./data-views/dashboard-data-riders/data-riders-resolve";
import {SocketService} from "../shared/utility-services/socket.service";
import {
  NotificationSidebarComponent,
  NotificationSidebarService
} from './notification-sidebar/notification-sidebar.component';
import {BootstrapDateTimePickerModule} from "../shared/utility-modules/date-time-picker-module";

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    DashboardRoutingModule,
    TextMaskModule,
    NgbModalModule,
    WizardModule,
    BootstrapTourModule,
    CtsAppDragDropModule,
    BootstrapDateTimePickerModule
  ],
  entryComponents: [
    BasicModalComponent,
    RoutedModalComponent,
    InputsModalComponent
  ],
  declarations: [
    DashboardComponent,
    DashboardAdminComponent,
    DashboardHomeComponent,
    DashboardHomeView1Component,
    DashboardHomeView2Component,
    SidebarComponent,
    NavbarComponent,
    DashboardDataRidersComponent,
    DashboardDataTransactionsComponent,
    DashboardDataPassesComponent,
    DashboardToastComponent,
    DashboardTypeaheadComponent,
    TypeaheadComponent,
    DashboardFormsComponent,
    LoadingIndicatorComponent,
    DashboardLoadingIndicatorComponent,
    DashboardModalComponent,
    BasicModalComponent,
    RoutedModalComponent,
    InputsModalComponent,
    DashboardDatepickerComponent,
    DashboardWizardComponent,
    DashboardDragDropComponent,
    NotificationSidebarComponent
  ],
  providers: [
    DashboardAuthGuard,
    DashboardAdminAuthGuard,
    DataRidersResolve,
    RiderService,
    TransactionService,
    PassService,
    SidebarService,
    MaskService,
    NgbModalStack,
    SocketService,
    NotificationSidebarService
  ]
})
export class DashboardModule { }
