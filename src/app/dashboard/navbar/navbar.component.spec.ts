/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { NavbarComponent } from './navbar.component';
import {SidebarService} from "../sidebar/sidebar.component";
import {AuthService} from "../../shared/auth/auth.service";
import {NotificationSidebarService} from "../notification-sidebar/notification-sidebar.component";

let mockAuthServiceStub = {
  getLoggedInUser: function() {
    return {
      isAdmin: false
    }
  }
}

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarComponent ],
      providers: [SidebarService, NotificationSidebarService, {provide: AuthService, useValue: mockAuthServiceStub }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
