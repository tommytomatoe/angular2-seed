import { Component, OnInit } from '@angular/core';
import {SidebarService} from "../sidebar/sidebar.component";
import {LoggedInUser} from "../../shared/auth/iauth.service";
import {AuthService} from "../../shared/auth/auth.service";
import {NotificationSidebarService} from "../notification-sidebar/notification-sidebar.component";

@Component({
  selector: '[app-navbar]',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  loggedInUser: LoggedInUser;

  constructor(
    private sidebarService: SidebarService,
    private authService: AuthService,
    private notificationSidebarService: NotificationSidebarService
  ) { }

  ngOnInit() {
    this.loggedInUser = this.authService.getLoggedInUser();
  }

  // ------------------------------------------------------------------- UI Events

  openSidebarClicked() {
    this.sidebarService.isOpen = true;
  }

  changeUserPermissionToNormalClicked() {
    this.authService.changeLoggedInUserPermissionToNormal();
  }

  changeUserPermissionToAdminClicked() {
    this.authService.changeLoggedInUserPermissionToAdmin();
  }

  toggleNotificationSidebarClicked() {
    this.notificationSidebarService.isOpen = !this.notificationSidebarService.isOpen;
  }

}
