import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {DashboardHomeComponent} from "./dashboard-home/dashboard-home.component";
import {DashboardComponent} from "./dashboard.component";
import {DashboardAdminComponent} from "./dashboard-admin/dashboard-admin.component";
import {DashboardAdminAuthGuard} from "./dashboard-admin-auth-guard.service";
import {DashboardAuthGuard} from "./dashboard-auth-guard.service";
import {DashboardDataRidersComponent} from "./data-views/dashboard-data-riders/dashboard-data-riders.component";
import {DashboardDataPassesComponent} from "./data-views/dashboard-data-passes/dashboard-data-passes.component";
import {DashboardDataTransactionsComponent} from "./data-views/dashboard-data-transactions/dashboard-data-transactions.component";
import {DashboardToastComponent} from "./features/dashboard-toast/dashboard-toast.component";
import {DashboardTypeaheadComponent} from "./features/dashboard-typeahead/dashboard-typeahead.component";
import {DashboardFormsComponent} from "./features/dashboard-forms/dashboard-forms.component";
import {DashboardLoadingIndicatorComponent} from "./features/dashboard-loading-indicator/dashboard-loading-indicator.component";
import {DashboardModalComponent} from "./features/dashboard-modal/dashboard-modal.component";
import {DashboardDatepickerComponent} from "./features/dashboard-datepicker/dashboard-datepicker.component";
import {DashboardWizardComponent} from "./features/dashboard-wizard/dashboard-wizard.component";
import {DashboardDragDropComponent} from "./features/dashboard-drag-drop/dashboard-drag-drop.component";
import {DataRidersResolve} from "./data-views/dashboard-data-riders/data-riders-resolve";

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [DashboardAuthGuard],
        children: [
          {
            path: 'admin',
            canActivate: [DashboardAdminAuthGuard],
            component: DashboardAdminComponent
          },
          {
            path: 'home',
            component: DashboardHomeComponent
          },
          {
            path: 'toast',
            component: DashboardToastComponent
          },
          {
            path: 'typeahead',
            component: DashboardTypeaheadComponent
          },
          {
            path: 'forms',
            component: DashboardFormsComponent
          },
          {
            path: 'loading',
            component: DashboardLoadingIndicatorComponent
          },
          {
            path: 'modal',
            component: DashboardModalComponent
          },
          {
            path: 'modal/routed-modal',
            component: DashboardModalComponent
          },
          {
            path: 'datepicker',
            component: DashboardDatepickerComponent
          },
          {
            path: 'wizard',
            component: DashboardWizardComponent
          },
          {
            path: 'drag-and-drop',
            component: DashboardDragDropComponent
          },
          {
            // Empty route
            path: 'data',
            children: [
              {
                path: 'riders',
                component: DashboardDataRidersComponent,
                // Resolve riders before passing to this component
                resolve: {
                  riders: DataRidersResolve
                }
              },
              {
                path: 'passes',
                component: DashboardDataPassesComponent
              },
              {
                path: 'transactions',
                component: DashboardDataTransactionsComponent
              },
              {
                path: '',
                redirectTo: 'riders',
                pathMatch: 'full'
              }
            ]
          },
          {
            path: '',
            redirectTo: 'home',
            pathMatch: 'full'
          }
        ]
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class DashboardRoutingModule { }
