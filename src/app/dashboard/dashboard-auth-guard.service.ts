// Prevents the dashboard routes if the user is not logged in.

import { Injectable }     from '@angular/core';
import {CanActivate, Router}    from '@angular/router';
import {AuthService} from "../shared/auth/auth.service";

@Injectable()
export class DashboardAuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  canActivate() {
    let user = this.authService.getLoggedInUser();
    if (!user) {
      // The user is not logged in. Redirect to login.
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
