// Prevents the user from activating admin routes
// if they are not admin.

import { Injectable }     from '@angular/core';
import {CanActivate, Router}    from '@angular/router';
import {AuthService} from "../shared/auth/auth.service";

@Injectable()
export class DashboardAdminAuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  canActivate() {
    let user = this.authService.getLoggedInUser();
    if (!user || !user.isAdmin) {
      // Either the user is not logged in or is not admin.
      // Redirect to home.
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }
}
