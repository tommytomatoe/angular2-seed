import { Component, OnInit } from '@angular/core';
import {TransactionService} from "../../../shared/api/transaction.service";
import {ErrorService} from "../../../shared/utility-services/error.service";
import {Transaction} from "../../../shared/api/transaction.model";

@Component({
  selector: 'app-dashboard-home-view2',
  templateUrl: './dashboard-home-view2.component.html',
  styleUrls: ['./dashboard-home-view2.component.scss']
})
export class DashboardHomeView2Component implements OnInit {

  transactionProperties = Transaction.getProperties();
  transactions: Transaction[] = [];

  constructor(
    private transactionService: TransactionService,
    private errorService: ErrorService
  ) { }

  ngOnInit() {
    this.transactionService.getAll().subscribe((transactions) => {
      this.transactions = transactions;
    }, err => {
      if (err.status === 0) {
        // The mock server was unreachable, display a message
        this.errorService.serverUnreachable()
      } else {
        this.errorService.unknown(err);
      }
    });
  }

}
