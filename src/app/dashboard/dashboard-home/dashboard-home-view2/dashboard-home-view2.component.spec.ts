/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DashboardHomeView2Component } from './dashboard-home-view2.component';
import {TransactionService} from "../../../shared/api/transaction.service";
import {ErrorService} from "../../../shared/utility-services/error.service";
import {Observable} from "rxjs";

class TransactionServiceStub {
  getAll() {
    return new Observable<any>((subscriber) => {
      subscriber.next([]);
      subscriber.complete();
    })
  }
}
class ErrorServiceStub {

}

describe('DashboardHomeView2Component', () => {
  let component: DashboardHomeView2Component;
  let fixture: ComponentFixture<DashboardHomeView2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardHomeView2Component ],
      providers: [
        {provide: TransactionService, useClass: TransactionServiceStub},
        {provide: ErrorService, useClass: ErrorServiceStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardHomeView2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
