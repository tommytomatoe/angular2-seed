/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DashboardHomeComponent } from './dashboard-home.component';
import {BootstrapTourModule} from "../../shared/utility-directives/bootstrap-tour.module";
import {RouterOutletStubComponent} from "../../../testing/router-stub";
import {SocketService} from "../../shared/utility-services/socket.service";

export class SocketServiceStub {

  constructor() { }

  listenForEvent(event: string, callback: Function) { }

  stopListeningForEvent(event: string) { }

}

describe('DashboardHomeComponent', () => {
  let component: DashboardHomeComponent;
  let fixture: ComponentFixture<DashboardHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BootstrapTourModule],
      declarations: [ DashboardHomeComponent, RouterOutletStubComponent ],
      providers: [
        {provide: SocketService, useClass: SocketServiceStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
