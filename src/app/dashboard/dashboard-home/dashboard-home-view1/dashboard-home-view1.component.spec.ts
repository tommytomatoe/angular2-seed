/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DashboardHomeView1Component } from './dashboard-home-view1.component';
import {Observable} from "rxjs";
import {RiderService} from "../../../shared/api/rider.service";
import {ErrorService} from "../../../shared/utility-services/error.service";

class RiderServiceStub {
  getAll() {
    return new Observable<any>((subscriber) => {
      subscriber.next([]);
      subscriber.complete();
    })
  }
}
class ErrorServiceStub {

}

describe('DashboardHomeView1Component', () => {
  let component: DashboardHomeView1Component;
  let fixture: ComponentFixture<DashboardHomeView1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardHomeView1Component ],
      providers: [
        {provide: RiderService, useClass: RiderServiceStub},
        {provide: ErrorService, useClass: ErrorServiceStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardHomeView1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
