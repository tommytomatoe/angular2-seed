import { Component, OnInit } from '@angular/core';
import {RiderService} from "../../../shared/api/rider.service";
import {ErrorService} from "../../../shared/utility-services/error.service";
import {Rider} from "../../../shared/api/rider.model";

@Component({
  selector: 'app-dashboard-home-view1',
  templateUrl: './dashboard-home-view1.component.html',
  styleUrls: ['./dashboard-home-view1.component.scss']
})
export class DashboardHomeView1Component implements OnInit {

  riderProperties = Rider.getProperties();
  riders: Rider[] = [];

  constructor(
    private riderService: RiderService,
    private errorService: ErrorService
  ) { }

  ngOnInit() {
    this.riderService.getAll().subscribe((riders) => {
      this.riders = riders;
    }, err => {
      if (err.status === 0) {
        // The mock server was unreachable, display a message
        this.errorService.serverUnreachable();
      } else {
        this.errorService.unknown(err);
      }
    });
  }

}
