import { Component, OnInit } from '@angular/core';
import {BootstrapTourService} from "../../shared/utility-directives/bootstrap-tour.module";
import {SocketService} from "../../shared/utility-services/socket.service";

@Component({
  selector: 'app-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.scss']
})
export class DashboardHomeComponent implements OnInit {

  num1 = 0;
  num2 = 0;
  num3 = 0;
  num4 = 0;

  constructor(
    private bootstrapTourService: BootstrapTourService,
    private socketService: SocketService
  ) { }

  ngOnInit() {
    this.socketService.listenForEvent('num1', (data) => {
      this.num1 = data;
    });
    this.socketService.listenForEvent('num2', (data) => {
      this.num2 = data;
    });
    this.socketService.listenForEvent('num3', (data) => {
      this.num3 = data;
    });
    this.socketService.listenForEvent('num4', (data) => {
      this.num4 = data;
    });
  }

  ngOnDestroy() {
    this.socketService.stopListeningForEvent('num1');
    this.socketService.stopListeningForEvent('num2');
    this.socketService.stopListeningForEvent('num3');
    this.socketService.stopListeningForEvent('num4');
  }

  startFeatureTourClicked() {
    this.bootstrapTourService.start();
  }

}
