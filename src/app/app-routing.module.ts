import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {LandingPageComponent} from "./landing-page/landing-page.component";
import {LoginComponent} from "./authenticate/login/login.component";
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [
    FormsModule,
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent },
      { path: '', component: LandingPageComponent }
    ])
  ],
  declarations: [
    LoginComponent,
    LandingPageComponent,
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
