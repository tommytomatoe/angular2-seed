// These files are third party non-Angular - we require them here as well as in angular-cli.json
declare const require;
require('assets/vendor/moment.min.js');
require('assets/vendor/jquery.min.js');
require('assets/vendor/bootstrap.min.js');
require('assets/vendor/bootstrap-tour.js');
require('eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');

export * from './app.component';
export * from './app.module';
