import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {AuthService} from "../../shared/auth/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  submitted = false;
  authDetails = {
    email: '',
    password: ''
  }

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  // ------------------------------------------------------------------- Form Submission - Sign In
  onSubmit() {
    this.submitted = true;
    this.authService.signIn(this.authDetails.email, this.authDetails.password).subscribe((loggedInUser) => {
    	this.router.navigate(['dashboard']);
    }, error => {
      // Since we're using a mock auth service by default, and your errors will probably be specific,
      // we'll log the error to the console here
      console.log(error);
    });
  }

  // ------------------------------------------------------------------- Sign in

  signInWithGoogleClicked() {
    if (!this.authService.getGoogleClientAuthId()) {
      alert('For this to work, you\'ll need to supply your own Google Client Auth ID in the Mock Auth Service.')
    } else {
      this.authService.signInWithGoogle().subscribe((loggedInUser) => {
        this.router.navigate(['dashboard']);
      }, error => {
        // Since we're using a mock auth service by default, and your errors will probably be specific,
        // we'll log the error to the console here
        console.log(error);
      });
    }
  }

}
