/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import { LoginComponent } from './login.component';
import {FormsModule} from "@angular/forms";
import {AuthService} from "../../shared/auth/auth.service";
import {CommonModule} from "@angular/common";
import {Router} from "@angular/router";
import {RouterStub} from "../../../testing/router-stub";

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  let authServiceStub: { }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      providers:[
          CommonModule,
          {provide: AuthService, useValue: authServiceStub },
          { provide: Router, useClass: RouterStub }
        ],
      imports: [FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not show error for valid email and password', () => {
    component.authDetails.email = 'test@test.com';
    component.authDetails.password = '123';

    let de = fixture.debugElement.query(By.css('#loginButton'));
    let disabledProperty = de.properties['disabled'];
    expect(disabledProperty).toBe(false);
  });
});


